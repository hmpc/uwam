/*
	mapper.v	- symbol mapper module for transmitter

	The symbol mapper maps channel-coded words to symbols for the modulator. It
	may perform Gray coding or implement sophisticated encoding methods like
	TCM. The mapping occurs on the rising edge of wck, and symbol is updated on
	the next clock cycle. If disabled, it becomes transparent.

	Created:	2014-03-11 16:48:51 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module mapper #(
	parameter integer	SYM_WIDTH	= 1,
	parameter integer	WORD_SIZE	= 1
) (
	/* Module control */
	input	clk,
	input	reset,
	input	enable,

	/* Main operation I/O */
	input							word_clk,
	input		[WORD_SIZE-1: 0]	word,
	output	reg						sym_clk,
	output	reg	[SYM_WIDTH-1: 0]	symbol
);

localparam	SYM_FILL	= SYM_WIDTH - WORD_SIZE;

wire	[WORD_SIZE-1: 0]	gray;

/* Binary to Gray code mapping can be done efficiently by w ^ (w >> 1) */
generate
	if( WORD_SIZE == 1 )
		begin
			assign	gray = word;
		end
	else
		begin
			assign	gray = word ^ { 1'b0, word[ WORD_SIZE-1: 1 ] };
		end
endgenerate

reg		prev_wck;

always @( posedge clk )
begin
	if( reset )
		begin
			symbol		<= 0;
			sym_clk		<= 0;
			prev_wck	<= 0;
		end
	else if( enable )
		begin
			prev_wck	<= word_clk;
			if( prev_wck == 1'b0 && word_clk == 1'b1 )
				begin
					sym_clk	<= 1'b1;
					symbol	<= { {SYM_FILL{1'b0}}, gray };
				end
			else
				begin
					sym_clk	<= 1'b0;
				end
		end
	else
		begin
			symbol	<= { {SYM_FILL{1'b0}}, word };
		end
end

endmodule

