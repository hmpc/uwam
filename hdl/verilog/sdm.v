/*
	sdm.v	- 1st-order sigma delta modulator

	Created:	2014-03-21 13:31:36 (WET)
	By:			JCA <jca@fe.up.pt>
 */
module sdm #(
	parameter integer	N_BITS_IN	= 7,			/* Number of input bits */
	parameter integer	SIGNED		= 0,			/* din is signed if 1 */
	parameter integer	SAMPLE_HOLD	= 1,			/* Use internal S&H */
	parameter integer	CLK_FCY		= 100_000_000,	/* Clock frequency (Hz) */
	parameter integer	SH_FCY		= 1_000_000		/* S&H sampling frequency (Hz) */
) (
	input	clock,
	input	reset,

	input							din_en,	/* Data in enable for S&H */
	input		[N_BITS_IN-1: 0]	din,	/* Data in, synchronous with the posedge of clock */
	output	reg						dout	/* 1-bit output */
);

`include "log2.v"

localparam integer	CLK_DIV_R		= CLK_FCY / SH_FCY;
localparam integer	NBITS_CLK_DIV   = log2( CLK_DIV_R );


reg		[N_BITS_IN-1: 0]	din_reg;	/* Input register for S&H */

wire	[N_BITS_IN-1: 0]	sdin;		/* sign extended datain */

wire	[N_BITS_IN: 0]		idin;		/* sign extended datain */
wire	[N_BITS_IN: 0]		idout;		/* output after D/D converter */
wire	[N_BITS_IN: 0]		error;		/* S-D error */
wire						urdout;		/* data out unregistered */

reg		[N_BITS_IN: 0]		acc;		/* accumulator */

reg		[NBITS_CLK_DIV-1:0]	clockdiv;


/* Local sample & hold: */
always @(posedge clock)
begin
	if ( reset )
		begin
			din_reg		<= 32'd0;
			if ( SAMPLE_HOLD )
				clockdiv <= 32'd0;
		end
	else
		begin
			if ( SAMPLE_HOLD )
				begin
					if ( clockdiv == (CLK_DIV_R - 1) )
						begin
							din_reg		<= din;
							clockdiv <= 32'd0;
						end
					else
						clockdiv <= clockdiv + 1;
				end
			else
				if ( din_en )
					din_reg		<= din;
		end
end

/* temporary signals used for parametrization: */
wire	[64:0]	max_urdout, min_urdout, quantizer_th;

assign	min_urdout		= 2'b11 << (N_BITS_IN-1);
assign	max_urdout		= ~min_urdout;
assign	quantizer_th	= 1'b1 << N_BITS_IN;

assign	idout	= ( urdout ? (max_urdout) : (min_urdout) ); /* D/D converter */

/* convert unsigned input to 2's complement (toggle MSbit) */
assign	sdin	= SIGNED ? din_reg : ( { ~din_reg[N_BITS_IN-1], din_reg[N_BITS_IN-1-1:0]} );

/* sign extend din to (N_BITS_IN+1) bits */
assign	idin	= { sdin[N_BITS_IN-1], sdin};

assign	error	= idin - idout;

assign	urdout	= ~acc[N_BITS_IN];

/* Accumulator: */
always @(posedge clock)
begin
	if ( reset )
		acc		<= 0;
	else
		acc		<= acc + error;
end

/* Output register: */
always @(posedge clock)
begin
	if ( reset )
		dout	<= 0;
	else
		dout	<= urdout;
end

endmodule

