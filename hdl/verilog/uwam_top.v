/*
	uwam_top.v	- top-level module

	This module contains the top-level description of the system, including
	instantiation of the high-level components and auxiliary connections.

	Created:	2014-03-04 16:22:26 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module uwam_top (
	/* Global control */
	input	xtal,
	input	reset_n,

	/* LEDs */
	output	led3_n,
	output	led8_n,

	/* User switch */
	/* input	sw1, */

	/* Debugging display */
	output	dy1_rclk,
	output	dy1_sclk,
	output	dy1_ser,

	/* Flash interface (SPI) */
	/* input	flash_miso, */
	/* output	flash_mosi, */
	/* output	flash_sck, */
	/* output	flash_cs, */

	/* ADC (SPI) */
	input	adc_dout,
	output	adc_din,
	output	adc_sck,
	output	adc_cs,

	/* Potentiometers (I2C) */
	inout	pot_sda,
	inout	pot_scl,

	/* Differential signal output */
	output	sig_out_n,
	output	sig_out_p,
	output	sig_trig,

	/* External serial port */
	input	s3_rx,
	output	s3_tx,

	output	s4_tx
);

`include	"uwam.vh"
`include	"transmitter.vh"

/* Generic counter variable for initialisations */
integer	i;

/*
	Global control signals
 */
wire	clk;
wire	reset;

wire	dcm_locked;

assign	reset = ~reset_n | ~dcm_locked;

/* Generate main clock from on-board crystal */
dcm_2x dcm_main (
	.CLKIN_IN( xtal ),
	.RST_IN( ~reset_n ),
	.CLKIN_IBUFG_OUT( ),
	.CLK0_OUT( ),
	.CLK2X_OUT( clk ),
	.LOCKED_OUT( dcm_locked )
);


/*
	LEDs
 */
assign	led3_n	= 1'b1;
assign	led8_n	= 1'b1;


/*
	Debugging display
 */
wire	[ 3: 0]	dy1_d[ 2: 0];
wire	[ 2: 0]	dy1_dp;

oho_dy1 dy1 (
	.clock( clk ),
	.reset( reset ),
	.D0( dy1_d[0] ),
	.D1( dy1_d[1] ),
	.D2( dy1_d[2] ),
	.dp0( dy1_dp[0] ),
	.dp1( dy1_dp[1] ),
	.dp2( dy1_dp[2] ),
	.DY1_SCLK( dy1_sclk ),
	.DY1_SER( dy1_ser ),
	.DY1_RCLK( dy1_rclk )
);


/*
	External communication
 */
wire	uart_rx;
wire	uart_tx;

wire	uart_cts;

wire	uart_tx_en;
wire	uart_tx_rdy;
wire	uart_rx_rdy;

wire	[`IN_WORD_SIZE-1: 0]	uart_rx_data;
wire	[`IN_WORD_SIZE-1: 0]	uart_tx_data;

assign	uart_rx	= s3_rx;
assign	s3_tx	= uart_tx;

assign	s4_tx	= uart_cts;

suart_v1 #(
	.INPUT_CLOCK_FREQUENCY( `CLK_FCY )
) uart (
	.clock( clk ),
	.reset( reset ),
	.tx( uart_tx ),
	.rx( uart_rx ),
	.txen( uart_tx_en ),
	.txready( uart_tx_rdy ),
	.rxready( uart_rx_rdy ),
	.dout( uart_rx_data ),
	.din( uart_tx_data )
);


/*
	High-level transmitter
 */
wire	tx_sdi;
wire	tx_sck;
wire	tx_stop;

wire			tx_cfg_we;
wire			tx_cfg_update;
wire	[ 7: 0]	tx_cfg_addr;
wire	[15: 0]	tx_cfg_din;
wire	[15: 0]	tx_cfg_dout;

wire	[`TX_OUT_WIDTH-1: 0]	tx_sig_out;
wire							tx_sig_en;

transmitter #(
	.N_SYMBOLS( `N_SYMBOLS ),
	.PHASE_WIDTH( `TX_PHASE_WIDTH ),
	.OUT_WIDTH( `TX_OUT_WIDTH ),
	.CODE_SIZE( `TX_CODE_SIZE ),
	.SYMBOL_TIME( `TX_SYMBOL_TIME )
) tx (
	.clk( clk ),
	.reset( reset ),
	.enable( 1'b1 ),

	.sck( tx_sck ),
	.sdi( tx_sdi ),

	.cfg_we( tx_cfg_we ),
	.cfg_addr( tx_cfg_addr[3:0] ),
	.cfg_din( tx_cfg_din ),
	.cfg_dout( tx_cfg_dout ),
	.cfg_update( tx_cfg_update ),

	.sig_en( tx_sig_en ),
	.sig_out( tx_sig_out )
);


/*
	Flow control between Picoblaze and transmitter
 */
wire							flow_cts;
wire	[`IN_WORD_SIZE-1: 0]	flow_pdi;
wire							flow_pen;

tx_flow_ctrl #(
	.OUT_PERIOD( `TX_SYMBOL_TIME ),
	.WORD_SIZE( `IN_WORD_SIZE )
) flow (
	.clk( clk ),
	.reset( reset ),
	.enable( 1'b1 ),
	.pdi( flow_pdi ),
	.pen( flow_pen ),
	.sdo( tx_sdi ),
	.sen( tx_sck ),
	.cts( flow_cts ),
	.stop( tx_stop )
);

assign	uart_cts	= flow_cts;


/*
	I2C potentiometer interface
 */
wire			i2c_save_addr;
wire	[ 6: 0]	i2c_addr;

wire			i2c_write;
wire	[ 7: 0]	i2c_dout;
wire			i2c_read;
wire	[ 7: 0]	i2c_din;
wire			i2c_din_rdy;

wire			i2c_rdy;
wire			i2c_done;
wire			i2c_error;

wire			i2c_scl_out;
wire			i2c_sda_out;

i2c #(
	.CLK_IN( `CLK_FCY )
) pot (
	.clk( clk ),
	.rst( reset ),
	.save_addr( i2c_save_addr ),
	.addr( i2c_addr ),
	.write( i2c_write ),
	.data( i2c_dout ),
	.read( i2c_read ),
	.data_out_rdy( i2c_din_rdy ),
	.data_out( i2c_din ),
	.rdy( i2c_rdy ),
	.error( i2c_error ),
	.done( i2c_done ),
	.sda( i2c_sda_out ),
	.sda_in( pot_sda ),
	.scl( i2c_scl_out ),
	.scl_in( pot_scl )
);

assign	pot_scl	= ( i2c_scl_out ? 1'bz : 1'b0 );
assign	pot_sda	= ( i2c_sda_out ? 1'bz : 1'b0 );


/*
	ADC interface
 */
wire			adc_start;
wire	[ 1: 0]	adc_mode;
wire			adc_chan;
wire			adc_error;

wire			adc_ch1_rdy;
wire	[11: 0]	adc_ch1;
wire			adc_ch2_rdy;
wire	[11: 0]	adc_ch2;

adc_interface #(
	.NCLK1( `ADC_NCLK ),
	.NCLK2( `ADC_NCLK ),
	.NTQUIET( `ADC_NQUIET ),
	.NPOWERON( `ADC_NPOWERON )
) adc (
	.clk( clk ),
	.rst( reset ),
	.adc_out( adc_dout ),
	.adc_in( adc_din ),
	.scl( adc_sck ),
	.cs( adc_cs ),
	.start_in( adc_start ),
	.mode( adc_mode ),
	.ch1( adc_ch1 ),
	.ch1rdy( adc_ch1_rdy ),
	.ch2( adc_ch2 ),
	.ch2rdy( adc_ch2_rdy ),
	.adc_error( adc_error )
);

/* We never need to cycle through the channels */
assign	adc_mode	= { 1'b0, adc_chan };

/* Latch ADC output on rdy pulse for downstream */
reg		[11: 0]	adc_data;
wire			adc_data_rdy;

assign	adc_data_rdy	= adc_ch1_rdy | adc_ch2_rdy;

always @( posedge clk )
begin
	if( reset )
		adc_data		<= 0;
	else
		begin
			if( adc_ch1_rdy )
				adc_data	<= adc_ch1;

			if( adc_ch2_rdy )
				adc_data	<= adc_ch2;
		end
end


/*
	Polyphase decimation filter
 */
wire			aa_rfd;
reg				aa_din_rdy;
wire	[11: 0]	aa_din;
wire			aa_dout_rdy;
wire	[15: 0]	aa_dout;

bp_filter filter (
	.clk( clk ),
	.sclr( reset ),
	.nd( aa_din_rdy ),
	.rfd( aa_rfd ),
	.rdy( aa_dout_rdy ),
	.din( aa_din ),
	.dout( aa_dout )
);

assign	aa_din	= adc_data;

/* Assert ND when data arrives from the ADC, deassert when filter reads it */
reg				aa_rfd_d;

always @( posedge clk )
begin
	aa_rfd_d	<= aa_rfd;

	if( reset )
		aa_din_rdy	<= 0;
	else if( adc_data_rdy )
		aa_din_rdy	<= 1'b1;
	else if( ~aa_rfd & aa_rfd_d )
		aa_din_rdy	<= 1'b0;
end


/*
	Waveform memory for debugging
 */
wire			wave_start;
wire			wave_done;

wire			wave_sels;
wire			wave_din_rdy;
wire	[17: 0]	wave_din;

wire	[ 9: 0]	wave_addr;
wire	[17: 0]	wave_dout;

assign	wave_din_rdy	= wave_sels ? aa_dout_rdy : adc_data_rdy;
assign	wave_din		= wave_sels ? { 2'b0, aa_dout } : { 4'b0, adc_data, 2'b0 };

wave_mem wmem (
	.clk( clk ),
	.reset( reset ),
	.enable( 1'b1 ),
	.start( wave_start ),
	.done( wave_done ),
	.din_rdy( wave_din_rdy ),
	.din( wave_din ),
	.addr( wave_addr ),
	.dout( wave_dout )
);


/*
	Picoblaze for modem control
 */
wire	[ 9: 0]	pb_addr;
wire	[17: 0]	pb_instr;

wire	[ 7: 0]	pb_port;
wire			pb_write;
wire	[ 7: 0]	pb_out;
wire			pb_read;
reg		[ 7: 0]	pb_in;

reg				pb_int;
wire			pb_int_ack;

reg		[ 7: 0]	pb_in_ports[ 7: 0];
reg		[ 7: 0]	pb_out_ports[ 7: 0];

kcpsm3 pb (
	.clk( clk ),
	.reset( reset ),

	.address( pb_addr ),
	.instruction( pb_instr ),

	.port_id( pb_port ),
	.write_strobe( pb_write ),
	.out_port( pb_out ),
	.read_strobe( pb_read ),
	.in_port( pb_in ),

	.interrupt( pb_int ),
	.interrupt_ack( pb_int_ack )
);

/* Picoblaze code ROM */
pb_rom pb_code (
	.clk( clk ),
	.address( pb_addr ),
	.instruction( pb_instr )
);

/* Picoblaze interrupt control */
always @( posedge clk )
begin
	if( reset )
		pb_int	<= 0;
	else
		begin
			if( pb_int_ack )
				pb_int	<= 1'b0;
			else if( uart_rx_rdy )
				pb_int	<= 1'b1;
		end
end


/*
	Picoblaze/UART FIFO
 */
wire	[ 7: 0]	pfu_din;
wire			pfu_wr_en;
wire	[ 7: 0]	pfu_dout;
wire			pfu_rd_en;

wire			pfu_full;
wire			pfu_empty;

pb_fifo pb_uart_fifo (
	.clk( clk ),
	.rst( reset ),
	.din( pfu_din ),
	.wr_en( pfu_wr_en ),
	.rd_en( pfu_rd_en ),
	.dout( pfu_dout ),
	.full( pfu_full ),
	.empty( pfu_empty )
);

/* Delay pfu_rd_en to enable UART only after FIFO outputs word */
reg		pfu_rd_en_d;

always @( posedge clk )
begin
	if( reset )
		pfu_rd_en_d	<= 0;
	else
		pfu_rd_en_d	<= pfu_rd_en;
end

assign	pfu_rd_en		= uart_tx_rdy & ~pfu_rd_en_d & ~pfu_empty;
assign	uart_tx_en		= pfu_rd_en_d;
assign	uart_tx_data	= pfu_dout;


/*
	Picoblaze/I2C FIFO
 */
wire	[ 7: 0]	pfi_din;
wire			pfi_wr_en;
wire	[ 7: 0]	pfi_dout;
wire			pfi_rd_en;

wire			pfi_full;
wire			pfi_empty;

pb_fifo pb_i2c_fifo (
	.clk( clk ),
	.rst( reset ),
	.din( pfi_din ),
	.wr_en( pfi_wr_en ),
	.rd_en( pfi_rd_en ),
	.dout( pfi_dout ),
	.full( pfi_full ),
	.empty( pfi_empty )
);

/* Delay pfi_rd_en to enable I2C only after FIFO outputs word */
reg		pfi_rd_en_d;

always @( posedge clk )
begin
	if( reset )
		pfi_rd_en_d	<= 0;
	else
		pfi_rd_en_d	<= pfi_rd_en;
end

assign	pfi_rd_en	= i2c_rdy & ~pfi_rd_en_d & ~pfi_empty;
assign	i2c_write	= pfi_rd_en_d;
assign	i2c_dout	= pfi_dout;


/*
	Picoblaze I/O ports
 */
/* Input ports */
reg		[ 7: 0]	i2c_din_r;

always @( posedge clk )
begin
	if( i2c_din_rdy )
		i2c_din_r	<= i2c_din;
end

always @( posedge clk )
begin
	if( reset )
		begin
			pb_in	<= 0;
			for( i = 0; i < 8; i = i + 1 )
				pb_in_ports[i]	<= 0;
		end
	else
		begin
			pb_in_ports[0]	<= { 2'b0, pfu_full, uart_rx_rdy, i2c_error,
				i2c_din_rdy, pfi_full, i2c_done };
			pb_in_ports[1]	<= uart_rx_data;
			pb_in_ports[2]	<= tx_cfg_dout[7:0];
			pb_in_ports[3]	<= tx_cfg_dout[15:8];
			pb_in_ports[4]	<= i2c_din_r;

			pb_in			<= pb_in_ports[ pb_port[2:0] ];
		end
end

/* Output ports */
always @( posedge clk )
begin
	if( reset )
		begin
			for( i = 0; i < 8; i = i + 1 )
				pb_out_ports[i]	<= 0;
		end
	else if( pb_write )
		begin
			for( i = 0; i < 8; i = i + 1 )
				if( pb_port[i] )
					pb_out_ports[i]	<= pb_out;
		end
	else
		begin
			pb_out_ports[0][3:0]	<= 4'b0;
			pb_out_ports[6][7]		<= 1'b0;
		end
end

wire			pfu_wr_en_pb;
wire			pfu_wr_en_wave;
wire	[ 7: 0]	pfu_din_pb;
wire	[ 7: 0]	pfu_din_wave;

assign	pfu_wr_en	= ( adc_start ? pfu_wr_en_wave : pfu_wr_en_pb );
assign	pfu_din		= ( adc_start ? pfu_din_wave : pfu_din_pb );

assign	{ flow_pen, pfu_wr_en_pb }	= pb_out_ports[0][3:2];
assign	pfu_din_pb					= pb_out_ports[1];
assign	flow_pdi				= pb_out_ports[2];

assign	{ i2c_read, pfi_wr_en }		= pb_out_ports[0][1:0];
assign	{ i2c_save_addr, i2c_addr }	= pb_out_ports[6];
assign	pfi_din						= pb_out_ports[7];

assign	adc_chan	= pb_out_ports[0][4];
assign	adc_start	= pb_out_ports[0][5];
assign	wave_sels	= pb_out_ports[0][6];

assign	{ tx_cfg_update, tx_cfg_we }	= pb_out_ports[3][5:4];
assign	tx_cfg_addr						= pb_out_ports[3][3:0];
assign	tx_cfg_din						= { pb_out_ports[5], pb_out_ports[4] };

assign	dy1_d[0]	= pb_out_ports[0][7:4];
assign	dy1_dp		= { 2'b0, adc_error };


/*
	Output stored waveform on serial port every 1/2 second
 */
reg		[12: 0]	wave_ctrl;
reg		[24: 0]	wave_frame_cnt;
wire			wave_cnt_end;
wire			wave_selb;

assign	{ wave_cnt_end, wave_addr, wave_selb, pfu_wr_en_wave }	= wave_ctrl;
assign	pfu_din_wave	= ( wave_selb ? {1'b1, wave_dout[15:9]} : {1'b0, wave_dout[8:2]} );
assign	wave_start		= adc_start & ( wave_frame_cnt == 0 );

always @( posedge clk )
begin
	if( reset )
		wave_frame_cnt	<= 0;
	else if( adc_start )
		begin
			if( wave_frame_cnt == `CLK_FCY/2-1 )
				wave_frame_cnt	<= 0;
			else
				wave_frame_cnt	<= wave_frame_cnt + 1;
		end
end

always @( posedge clk )
begin
	if( reset )
		begin
			wave_ctrl	<= 13'h1000;
		end
	else if( wave_done )
		begin
			wave_ctrl	<= 0;
		end
	else if( ~wave_cnt_end & ~pfu_full )
		begin
			wave_ctrl	<= wave_ctrl + 1;
		end
end


/*
	Output signal using a square wave (MSb)
 */
tx_driver tx_out (
	.clk( clk ),
	.reset( reset ),
	.sig_en( tx_sig_en ),
	.sig( tx_sig_out[`TX_OUT_WIDTH-1] ),
	.out_p( sig_out_p ),
	.out_n( sig_out_n )
);

assign	sig_trig	= tx_sig_en;


endmodule

