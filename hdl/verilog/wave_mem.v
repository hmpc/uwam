/*
	wave_mem.v	- waveform memory module.

	Stores 1024 18-bit samples (i.e. one BRAM) of a waveform to be read out
	later. The module operation begins with the start signal going high, and new
	samples on din are acquired on din_rdy high. During acquisition, dout is
	constant and the addr input is unused.  Once the memory is filled, done
	pulses high for one cycle and the module reenters read mode. In this mode,
	the word at the location pointed by addr is output on the clock cycle after
	addr changes. Acquisition is disabled or stopped whenever enable is low, but
	the memory can still be read.

	Created:	2014-05-15 10:19:41 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module wave_mem (
	input				clk,
	input				reset,
	input				enable,

	input				start,
	output reg			done,
	input				din_rdy,
	input		[17: 0]	din,

	input		[ 9: 0]	addr,
	output reg	[17: 0]	dout
);

reg		[17: 0]	wave_mem	[1023: 0];

reg				state;
reg		[ 9: 0]	cnt;
reg				cnt_co;

integer	i;

initial
begin
	for( i = 0; i < 1024; i = i + 1 )
		wave_mem[i]	= 0;
end

always @( posedge clk )
begin
	if( reset )
		begin
			state	<= 0;
			cnt		<= 0;
			cnt_co	<= 0;
			done	<= 0;
			dout	<= 0;
		end
	else
		begin
			case( state )
				1'b0: begin
					if( start & enable )
						begin
							state	<= 1'b1;
						end

					done	<= 1'b0;
					cnt		<= 10'b0;
					cnt_co	<= 1'b0;
					dout	<= wave_mem[ addr ];
				end

				1'b1: begin
					if( cnt_co | ~enable )
						begin
							state	<= 1'b0;
						end
					else if( din_rdy )
						begin
							wave_mem[ cnt ]	<= din;
							{ cnt_co, cnt }	<= cnt + 1;
						end

					done	<= cnt_co;
				end
			endcase
		end
end

endmodule

