/*
	log2.v	- helper function to calculate base-2 logarithms.

	This function calculates the base-2 logarithm of the input integer and
	rounds up. Based on the existing code from Hélder Campos.

	Created:	2014-03-10 16:54:02 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
function integer log2( input integer in );
	integer tmp;
	begin
		tmp = in - 1;
		for( log2 = 0; tmp > 0; log2 = log2 + 1 )
			begin
				tmp = tmp >> 1;
			end
	end
endfunction
		
