/*
	config_interface.v	- configuration interface for external port.

	Interface between register-based configuration (as used on transmitter) and
	an external byte-oriented port (such as a serial port). Assumes word size
	of 16 bits (i.e. 16 bit wide configuration registers) and address size of
	8 bits.

	Created:	2014-03-19 14:49:08 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module config_interface (
	/* Module control */
	input				clk,
	input				reset,

	/* External port I/O */
	input				rx_new,
	input		[ 7: 0]	rx_data,
	input				tx_rdy,
	output	reg			tx_en,
	output	reg	[ 7: 0]	tx_data,

	/* Register I/O */
	output	reg	[ 7: 0]	reg_addr,
	input		[15: 0]	reg_din,
	output	reg	[15: 0]	reg_dout,
	output	reg			reg_we
);

localparam	IDLE			= 3'b000;
localparam	READ_WAIT		= 3'b001;
localparam	READ_LSB		= 3'b010;
localparam	READ_LSB_WAIT	= 3'b011;
localparam	READ_MSB		= 3'b100;
localparam	WRITE_LSB		= 3'b101;
localparam	WRITE_MSB		= 3'b110;


reg		[ 2: 0]	state;
reg		[ 7: 0]	reg_lsb;

always @( posedge clk )
begin
	if( reset )
		begin
			state		<= IDLE;
			tx_en		<= 0;
			tx_data		<= 0;
			reg_addr	<= 0;
			reg_dout	<= 0;
			reg_we		<= 0;

			reg_lsb		<= 0;
		end
	else
		begin
			case( state )
				IDLE:
					begin
						tx_en	<= 1'b0;
						reg_we	<= 1'b0;

						if( rx_new )
							begin
								reg_addr	<= { 1'b0, rx_data[6:0] };
								state		<= rx_data[7] ? READ_WAIT : WRITE_LSB;
							end
					end

				READ_WAIT:
					begin
						state	<= READ_LSB;
					end

				READ_LSB:
					begin
						tx_data		<= reg_din[7:0];
						tx_en		<= 1'b1;
						state		<= READ_LSB_WAIT;
					end

				READ_LSB_WAIT:
					begin
						tx_en		<= 1'b0;
						if( tx_rdy )
							begin
								tx_data		<= reg_din[15:8];
								state		<= READ_MSB;
							end
					end

				READ_MSB:
					begin
						tx_en		<= 1'b1;
						if( tx_rdy )
							begin
								state		<= IDLE;
							end
					end

				WRITE_LSB:
					begin
						if( rx_new )
							begin
								reg_lsb	<= rx_data;
								state	<= WRITE_MSB;
							end
					end

				WRITE_MSB:
					begin
						if( rx_new )
							begin
								reg_dout	<= { rx_data, reg_lsb };
								reg_we		<= 1'b1;
								state		<= IDLE;
							end
					end

				default:
					begin
						state	<= IDLE;
					end
			endcase
		end
end

endmodule

