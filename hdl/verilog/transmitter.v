/*
	transmitter.v	- high-level transmitter module

	This is the high-level transmitter module, one of the main digital modem
	components (others being the receiver and the protocol stack).  It receives
	a serial stream of bits and performs all coding, mapping, and modulation
	operations.  The transmitter can be configured through a register-based
	interface (see documentation for details).

	Created:	2014-03-05 10:52:00 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`include "transmitter.vh"

module transmitter #(
	parameter integer	N_SYMBOLS	= 2,
	parameter integer	PHASE_WIDTH	= 24,
	parameter integer	OUT_WIDTH	= 14,
	parameter integer	CODE_SIZE	= 1,
	parameter integer	SYMBOL_TIME	= 100_000
) (
	/* Global control */
	input	clk,
	input	reset,
	input	enable,

	/* Input data to transmit */
	input	sck,
	input	sdi,

	/* Configuration interface */
	input								cfg_we,
	input		[`CFG_ADDR_SIZE-1: 0]	cfg_addr,
	input		[`CFG_REG_SIZE-1: 0]	cfg_din,
	output	reg	[`CFG_REG_SIZE-1: 0]	cfg_dout,
	input								cfg_update,

	/* Analog output */
	output								sig_en,
	output		[OUT_WIDTH-1: 0]		sig_out
);

`include "uwam.vh"
`include "log2.v"

localparam integer	SYM_WIDTH	= 1 + log2( N_SYMBOLS );


/*
	Configuration interface
 */
localparam integer	CFG_REG_SIZE	= `CFG_REG_SIZE;
localparam integer	CFG_REG_NO		= ( 1 << `CFG_ADDR_SIZE );

reg		[CFG_REG_SIZE-1: 0]	cfg_regs [CFG_REG_NO-1: 0];

integer	i;

initial begin
	for( i = 0; i < CFG_REG_NO; i = i + 1 )
		begin
			cfg_regs[i]	= 0;
		end
end

always @( posedge clk )
begin
	if( cfg_we )
		begin
			cfg_regs[ cfg_addr ] <= cfg_din;
		end
	cfg_dout	<= cfg_regs[ cfg_addr ];
end


/*
	Transmission processing chain
 */

/* Channel coding */
wire	code_sck;
wire	code_sda;

assign	code_sck	= sck;
assign	code_sda	= sdi;

/* Serial-to-parallel conversion */
wire						word_clk;
wire	[CODE_SIZE-1: 0]	cur_word;

s2p #(
	.OUT_WIDTH( CODE_SIZE ),
	.OCLK_CYCLES( 2 )
) tx_s2p (
	.clk( clk ),
	.reset( reset ),
	.sck( code_sck ),
	.sdi( code_sda ),
	.out_clk( word_clk ),
	.out_data( cur_word )
);

/* Symbol mapping */
wire						sym_clk;
wire	[SYM_WIDTH-1: 0]	cur_sym;

mapper #(
	.SYM_WIDTH( SYM_WIDTH ),
	.WORD_SIZE( CODE_SIZE )
) tx_mapper (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.word_clk( word_clk ),
	.word( cur_word ),
	.sym_clk( sym_clk ),
	.symbol( cur_sym )
);

/* Modulation */
wire	[PHASE_WIDTH-1: 0]	dds_pinc;
wire	[PHASE_WIDTH-1: 0]	dds_poff;

wire						mod_pinc_en;
wire						mod_update;
wire	[SYM_WIDTH-1: 0]	mod_map_sym;
wire	[PHASE_WIDTH-1: 0]	mod_map_dth;

assign	dds_poff	= 0;

assign	mod_update	= cfg_update;
assign	mod_map_sym	= cfg_regs[ `MOD_MAP_SYM ];
assign	mod_map_dth	= {
	cfg_regs[ `MOD_MAP_DTH_H ][ (PHASE_WIDTH-CFG_REG_SIZE-1) : 0 ],
	cfg_regs[ `MOD_MAP_DTH_L ]
};

modulator #(
	.N_SYMBOLS( N_SYMBOLS ),
	.SYM_WIDTH( SYM_WIDTH ),
	.PHASE_WIDTH( PHASE_WIDTH ),
	.SYMBOL_TIME( SYMBOL_TIME )
) tx_mod (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.sym_clk( sym_clk ),
	.symbol( cur_sym ),
	.pinc_en( mod_pinc_en ),
	.pinc( dds_pinc ),
	.update( mod_update ),
	.map_sym( mod_map_sym ),
	.map_dth( mod_map_dth )
);

/* Frequency generation and output */
wire						dds_clr;
wire						dds_rdy;
wire	[PHASE_WIDTH-1: 0]	dds_phase;
wire	[OUT_WIDTH-1: 0]	dds_out;
wire						dds_ce;

assign	dds_ce	= ( mod_pinc_en ? 1'b1 : dds_out[OUT_WIDTH-1] );

dds tx_dds (
	.ce( dds_ce ),
	.clk( clk ),
	.sclr( reset ),
	.pinc_in( dds_pinc ),
	.poff_in( dds_poff ),
	.rdy( dds_rdy ),
	.sine( dds_out ),
	.phase_out( dds_phase )
);

assign	sig_en	= mod_pinc_en;
assign	sig_out	= ( sig_en ? dds_out : 0 );

endmodule

