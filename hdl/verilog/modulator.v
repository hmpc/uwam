/*
	modulator.v	- modulator module for transmitter

	The modulator converts symbols (0 to M-1) to phase increments, used by the
	downstream DDS core to generate frequencies. The mapping from symbols to
	frequencies (phase increments) is externally programmable. Symbols are
	clocked in on the rising edge of sym_clk, and the output phase increment is
	valid as long as pinc_en is high (used as DDS CE).

	Created:	2014-03-07 17:32:11 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module modulator #(
	parameter integer	N_SYMBOLS	= 2,
	parameter integer	SYM_WIDTH	= 1,
	parameter integer	PHASE_WIDTH	= 24,
	parameter integer	SYMBOL_TIME	= 100_000
) (
	/* Module control */
	input	clk,
	input	reset,
	input	enable,

	/* Main operation I/O */
	input							sym_clk,
	input		[SYM_WIDTH-1: 0]	symbol,
	output reg						pinc_en,
	output		[PHASE_WIDTH-1: 0]	pinc,

	/* Frequency mapping parameters */
	input						update,
	input	[SYM_WIDTH-1: 0]	map_sym,	/* Symbol to map */
	input	[PHASE_WIDTH-1: 0]	map_dth		/* Frequency in dTheta */
);

`include "log2.v"

localparam integer	SYM_TIME_WIDTH	= 1 + log2( SYMBOL_TIME );


reg		[PHASE_WIDTH-1: 0]	map [N_SYMBOLS-1: 0];

/* Update parameters */
integer s;

always @( posedge clk )
begin
	if( reset )
		begin
			for( s = 0; s < N_SYMBOLS; s = s + 1 )
				begin
					map[s]	<= 0;
				end
		end
	else if( update )
		begin
			map[ map_sym ]	<= map_dth;
		end
end


/* Calculate output phase increment */
reg		[SYM_WIDTH-1: 0]	cur_sym;
reg							prev_sym_clk;
reg							new_sym;

always @( posedge clk )
begin
	if( reset )
		begin
			cur_sym			<= 0;
			prev_sym_clk	<= 0;
		end
	else if( enable )
		begin
			prev_sym_clk	<= sym_clk;
			if( prev_sym_clk == 1'b0 && sym_clk == 1'b1 )
				begin
					cur_sym	<= symbol;
					new_sym	<= 1'b1;
				end
			else
				begin
					new_sym	<= 1'b0;
				end
		end
end

assign	pinc	= map[ cur_sym ];


/* Symbol timing */
reg		[SYM_TIME_WIDTH-1: 0]	sym_time;

always @( posedge clk )
begin
	if( reset )
		begin
			sym_time	<= SYMBOL_TIME;
			pinc_en		<= 0;
		end
	else if( enable )
		begin
			if( new_sym )
				begin
					pinc_en		<= 1'b1;
					sym_time	<= 1;
				end
			else if( sym_time <= SYMBOL_TIME )
				begin
					sym_time	<= sym_time + 1;
				end
			else
				begin
					pinc_en		<= 1'b0;
				end
		end
	else
		begin
			pinc_en		<= 1'b0;
		end
end

endmodule

