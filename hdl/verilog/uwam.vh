/*
	uwam.vh	- common UWAM definitions.

	Created:	2014-03-14 10:55:35 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`define	CLK_FCY			98_304_000

`define	IN_WORD_SIZE	8

`define	N_SYMBOLS		2

/* Transmitter constants */
`define	TX_PHASE_WIDTH	24
`define	TX_OUT_WIDTH	14
`define	TX_CODE_SIZE	1
`define	TX_SYMBOL_TIME	65_536		/* in clock cycles */

/* Sigma-delta modulator constants */
`define	SDM_N_BITS		8
`define	SDM_SIGNED		1
`define	SDM_SH_FCY		(`CLK_FCY/100)

/* ADC constants */
`define	ADC_FS			768_000
`define	ADC_NQUIET		32
`define	ADC_NCLK		( (`CLK_FCY/`ADC_FS - `ADC_NQUIET)/32 )
`define	ADC_NPOWERON	99

