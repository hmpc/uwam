/*
	s2p.v	- serial to parallel conversion module.

	Converts input serial data to parallel output (LSb-first). Also produces
	a clock signal for the downstream modules, with pulses of the specified
	width (OCLK_CYCLES), to indicate a new parallel sample ready.

	Created:	2014-03-12 10:44:02 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module s2p #(
	parameter integer	OUT_WIDTH	= 1,
	parameter integer	OCLK_CYCLES	= 5
) (
	/* Module control */
	input	clk,
	input	reset,

	input	sck,
	input	sdi,

	output							out_clk,
	output	reg	[OUT_WIDTH-1: 0]	out_data
);

`include "log2.v"

localparam	CCNT_WIDTH	= 1 + log2( OCLK_CYCLES );	/* Clock counter width */
localparam	DCNT_WIDTH	= 1 + log2( OUT_WIDTH );	/* Data counter width */


wire	[OUT_WIDTH-1: 0]	next_data;
reg		[OUT_WIDTH-1: 0]	cur_data;

generate
	if( OUT_WIDTH > 1 )
		begin
			assign	next_data	= { sdi, cur_data[OUT_WIDTH-1: 1] };
		end
	else
		begin
			assign	next_data	= sdi;
		end
endgenerate


reg		[CCNT_WIDTH-1: 0]	out_clk_cnt;	/* Output clock cycle counter */
reg		[DCNT_WIDTH-1: 0]	data_cnt;		/* Input data counter */

reg		prev_sck;	/* Previous SCK value for edge detection */
reg		pulse;		/* Synchronous copy of output clock */

always @( posedge clk )
begin
	if( reset )
		begin
			out_data	<= 0;

			prev_sck	<= 0;
			pulse		<= 0;
			cur_data	<= 0;
			data_cnt	<= 0;
		end
	else
		begin
			prev_sck	<= sck;
			if( prev_sck == 1'b0 && sck == 1'b1 )
				begin
					cur_data	<= next_data;
					data_cnt	<= data_cnt + 1;
				end

			if( data_cnt >= OUT_WIDTH )
				begin
					out_data	<= cur_data;
					data_cnt	<= 0;
					pulse		<= 1'b1;
				end

			if( out_clk_cnt >= OCLK_CYCLES - 1 )
				begin
					pulse		<= 1'b0;
				end
		end
end

always @( posedge clk )
begin
	if( reset )
		begin
			out_clk_cnt	<= 0;
		end
	else if( pulse )
		begin
			out_clk_cnt	<= out_clk_cnt + 1;
		end
	else
		begin
			out_clk_cnt	<= 0;
		end
end

assign	out_clk	= pulse;

endmodule

