`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer: Helder Campos
//
// Create Date:    15:16:12 02/17/2011
// Design Name:
// Module Name:    i2c
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 	WARNING: The module does not check for different addresses in immediate reads or writes.
//		To change the address wait for done before issuing a new read or write command.
//
//		inputs: save_addr, latches input addr.
//					sda_in, scl_in: inputs to the i2c open colector lines.
//					sda, scl: 		i2c outputs(open colector).
//					addr[6:0]:		I2C device address.
//					data[7:0]:		data to send.
//					write:			latches data in and starts (or schedules) data transmission. (pulse)
//					read:				starts or schedules data reception. (pulse)
//		outputs:
//					rdy: 				module is ready to schedule new reception or transmission.
//					error:			I2C device did not acknowledge
//					done:				module has no pending comunications
//					data_out_rdy:	On the rising edge of this signal data_out has the new received data.
//					data_out[7:0]:		The received data
//////////////////////////////////////////////////////////////////////////////////
module i2c ( save_addr,
				addr,
				write,
				data,
				read,
				data_out_rdy,
				data_out,
				rdy,
				error,
				done,
				sda,
				sda_in,
				scl,
				scl_in,
				clk,
				rst );

/* CLK DIVISOR */
parameter CLK_IN = 49_152_000;
parameter CLK_I2C = 100_000; //+-
parameter CLKDIV_SIZE = 8;
parameter NCLK = (CLK_IN / CLK_I2C / 3);
parameter CLKDIV_LOAD = (1 << CLKDIV_SIZE) - NCLK;

input save_addr;
input [6:0] addr;
input write;
input [7:0] data;
input read;
output data_out_rdy;
output [7:0] data_out;
output rdy;
output error;
output done;
output sda;
input sda_in;
output scl;
input scl_in;
input clk;
input rst;


/*Data path*/
//inputs: rst_dp, sro_load_data, sro_load_addr, sro_data, sda_set, sda_reset, error_ce
//outputs: nack, data_out, end_byte
reg [CLKDIV_SIZE-1:0]clkcount;
reg scl_aux;
reg [9:0]sri;
reg [8:0]sro;
reg [1:0] scl_in_ffs;
reg scl_in_sync_delay;
reg sda, scl, error;
//reg [7:0] data_out;
reg rdy;
reg read_ff;
reg [7:0] data_ff;
reg [7:0] addr_ff;
reg read_end_ff;
reg sri_ce_aux;
//reg data_out_rdy;
wire read_end_pulse;
wire scl_in_sync;
wire scl_aux_in;
wire [CLKDIV_SIZE-1:0]clkcountinc;
wire clkcountco;
wire sri_ce, sro_ce;
wire scl_in_pulse;
wire nack;
wire end_byte;
wire rst_dp, sro_load_data, sro_load_addr;
wire [7:0] sro_data;
wire error_ce;
wire read_end;
wire sda_set, sda_reset;
wire sri_rst;
assign sri_rst = rst | rst_dp | end_byte;
assign scl_in_sync = &scl_in_ffs;
assign scl_in_pulse = scl_in_sync & (~scl_in_sync_delay);
assign {clkcountco, clkcountinc} = clkcount + 1;
assign scl_aux_in = ~(scl_aux | scl);
assign sro_ce = clkcountco & scl_aux_in;
assign sri_ce = scl_in_pulse;
assign nack = sri[0];
assign end_byte = ~sri[9];
assign read_end = ~(sri[8]) && read_ff;
assign read_end_pulse = read_end & ~read_end_ff;

assign data_out = sri[7:0];
assign data_out_rdy = read_end_pulse && sri_ce_aux;

always @(posedge clk) begin

	if(rst) begin
		read_end_ff <= 0;
//		data_out_rdy <= 0;
	end else begin
		read_end_ff <= read_end;
//		data_out_rdy <= read_end_pulse && sri_ce_aux;
	end

	//if(read_end_pulse) begin
	//	data_out <= sri[7:0];
	//end

	if(rst | rst_dp) begin
		scl_in_ffs <= 2'b11;
		scl_in_sync_delay <= 1;
	end else begin
		scl_in_ffs <= {scl_in, scl_in_ffs[1]};
		scl_in_sync_delay <= scl_in_sync;
	end

	if(rst | rst_dp | clkcountco) begin
		clkcount <= CLKDIV_LOAD;
	end else if ((~scl) | scl_in_sync) begin
		clkcount <= clkcountinc;
	end

	if(rst | rst_dp) begin
		scl <= 1;
		scl_aux <= 0;
	end else if(clkcountco) begin
		scl <= scl_aux;
		scl_aux <= scl_aux_in;
	end

	if(sro_load_data)begin
		sri_ce_aux <= 1;
	end else if(sri_rst) begin
		sri_ce_aux <= 0;
	end

	if(sri_rst) begin
		sri <= 10'h3FE;
	end else if(sri_ce) begin
		sri <= {sri[8:0], sda_in};
	end

	if(rst | rst_dp) begin
		sro <= 9'h1FF;
	end else if(sro_load_data) begin
		sro <= {sro_data, 1'b1};
	end else if(sro_load_addr) begin
		sro <= {addr_ff, 1'b1};
	end else if (sro_ce) begin
		sro <= {sro[7:0], 1'b0};
	end

	if(rst | sda_set) begin
		sda <= 1;
	end else if (sda_reset)begin
		sda <= 0;
	end else if(sro_ce) begin
		sda <= sro[8];
	end

end

always @(posedge clk) begin
	if(rst | read | write) begin
		error <= 0;
	end else if (error_ce) begin
		error <= 1;
	end

	if(rst | sro_load_data | error_ce) begin
		rdy <= 1;
	end else if(write | read)begin
		rdy <= 0;
	end

	if(sro_load_data) begin
		read_ff <= addr_ff[0];
	end

	if(write)
		data_ff <= data;

	if(save_addr)
		addr_ff[7:1] <= addr;

	if(write)
		addr_ff[0] <= 0;
	else if (read)
		addr_ff[0] <= 1;
end
/*datapath end*/

assign sro_data = addr_ff[0] ? 9'h1FF : data_ff;

parameter INIT = 3'b0,
			 SEND_ADDR = 3'd1,
			 SEND_DATA = 3'd2,
			 STOP =	3'd3,
			 STOP2 =	3'd4,
			 STOP3 =	3'd5;

reg [6:0]fsm_out;
reg [2:0]state = INIT;
reg [2:0]nextstate;
assign clk_down = (clkcountco && !scl_aux);
assign {done, error_ce, rst_dp, sro_load_addr, sro_load_data, sda_set, sda_reset} = fsm_out;
//inputs: nack, data_out, end_byte, sro_ce, rdy, read_ff, addr_ff[0]

always @(posedge clk) begin
	if(rst) begin
		state <= INIT;
	end else begin
		state <= nextstate;
	end
end

always @(*) begin
	case(state)
	INIT: begin
		if(!rdy) begin
			nextstate <= SEND_ADDR;
			fsm_out <= 7'b0001001;
		end else begin
			nextstate <= INIT;
			fsm_out <= 7'b1010010;
		end
	end
	SEND_ADDR: begin
		if(end_byte) begin
			if(!nack) begin
				nextstate <= SEND_DATA;
				fsm_out <= 7'b0000100;
			end else begin
				nextstate <= STOP;
				fsm_out <= 7'b0100000;
			end
		end else begin
			nextstate <= SEND_ADDR;
			fsm_out <= 7'b0000000;
		end
	end

	SEND_DATA: begin
		if(read_end & sro_ce) begin
			nextstate <= SEND_DATA;
			if((!rdy) && addr_ff[0]) begin
				fsm_out <= 7'b0000001;	//ACK
			end else begin
				fsm_out <= 7'b0000010;	//NACK
			end
		end else if(end_byte) begin
			if(read_ff) begin
				if(!(rdy) && addr_ff[0]) begin
					nextstate <= SEND_DATA;
					fsm_out <= 7'b0000100;
				end else begin
					nextstate <= STOP;
					fsm_out <= 7'b0000000;
				end
			end else begin
				if(!(nack || rdy || addr_ff[0])) begin
					nextstate <= SEND_DATA;
					fsm_out <= 7'b0000100;
				end else begin
					nextstate <= STOP;
					if(nack) begin
						fsm_out <= 7'b0100000;
					end else begin
						fsm_out <= 7'b0000000;
					end
				end
			end
		end else begin
			nextstate <= SEND_DATA;
			fsm_out <= 7'b0000000;
		end
	end
	STOP: begin
		if(sro_ce) begin
			nextstate <= STOP2;
			fsm_out <= 7'b0000001;
		end else begin
			nextstate <= STOP;
			fsm_out <= 7'b000000;
		end
	end
	STOP2: begin
		if (clk_down) begin
			nextstate <= STOP3;
			fsm_out <= 7'b0010010;
		end else begin
			nextstate <= STOP2;
			fsm_out <= 7'b000000;
		end
	end
	STOP3: begin
		if (clk_down) begin
			nextstate <= INIT;
			fsm_out <= 7'b0010010;
		end else begin
			nextstate <= STOP3;
			fsm_out <= 7'b000000;
		end
	end
	default: begin
		nextstate <= INIT;
		fsm_out <= 7'bx;
	end
	endcase
end

endmodule
