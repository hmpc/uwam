/*
	p2s.v	- parallel to serial conversion module.

	Converts input parallel data to serial output (LSb-first). The enable input
	can be used to control the output data rate. The read input is independent
	of the module enable, and will always latch in the value at din.

	Created:	2014-03-14 11:29:31 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module p2s #(
	parameter integer	WORD_SIZE	= 8
) (
	/* Module control */
	input	clk,
	input	reset,
	input	enable,
	input	read,

	/* Data I/O */
	input	[WORD_SIZE-1: 0]	din,
	output						dout
);

reg		[WORD_SIZE-1: 0]	data;

assign	dout	= ( enable ? data[0] : 0 );

always @( posedge clk )
begin
	if( reset )
		begin
			data	<= 0;
		end
	else
		begin
			if( enable )
				begin
					data	<= { 1'b0, data[ WORD_SIZE-1: 1 ] };
				end

			if( read )
				begin
					data	<= din;
				end
		end
end

endmodule

