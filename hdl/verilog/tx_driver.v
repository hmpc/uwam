/*
	tx_driver.v	- output module to drive transmission board.

	Produces complementary outputs to drive the input MOSFETs on the
	transmission board. The outputs are out of phase in such a way that the FETs
	are never on simultaneously, with a delay between edges of 2^DELAY_SIZE
	clock cycles to allow for complete switching . If the output is disabled
	(sig_en low), both outputs are low, turning off both transistors and
	avoiding capacitor discharge.

	Created:	2014-05-19 14:07:41 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module tx_driver #(
	parameter integer	DELAY_SIZE	= 5
) (
	input	clk,
	input	reset,

	input	sig_en,
	input	sig,

	output	out_p,
	output	out_n
);

reg		[DELAY_SIZE-1: 0]	out_cnt;
wire						clk_en_out;

reg			out_p_1;
reg			out_p_2;
reg			out_n_1;
reg			out_n_2;

always @( posedge clk )
begin
	if( reset )
		out_cnt	<= 0;
	else
		out_cnt	<= out_cnt + 1;
end

assign	clk_en_out	= ( out_cnt == 0 );

always @( posedge clk )
begin
	if( reset )
		begin
			out_p_1	<= 1'b0;
			out_p_2	<= 1'b0;

			out_n_1	<= 1'b0;
			out_n_2	<= 1'b0;
		end
	else if( clk_en_out )
		begin
			out_p_2	<= out_p_1;
			out_p_1	<= sig_en ? sig : 1'b0;

			out_n_2	<= out_n_1;
			out_n_1	<= sig_en ? ~sig : 1'b0;
		end
end

assign	out_p	= out_p_1 & out_p_2;
assign	out_n	= out_n_1 & out_n_2;

endmodule

