/*
	transmitter.vh	- common transmitter definitions

	Created:	2014-03-05 18:23:01 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */

/* Configuration interface parameters */
`define	CFG_ADDR_SIZE	4
`define	CFG_REG_SIZE	16

/* Configuration address map */
`define	MOD_MAP_SYM		0
`define	MOD_MAP_DTH_L	1
`define	MOD_MAP_DTH_H	2

