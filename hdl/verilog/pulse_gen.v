/*
	pulse_gen.v	- pulse generator module.

	Generates regular pulses (with one clock cycle width), with parametrisable
	period and offset. If disabled on the same cycle a pulse would be generated,
	the output remains low.

	Created:	2014-03-14 10:46:30 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module pulse_gen #(
	parameter integer	PERIOD	= 8,	/* Pulse period in clock cycles */
	parameter integer	OFFSET	= 0		/* Pulse offset in clock cycles */
) (
	/* Module control */
	input	clk,
	input	reset,
	input	enable,

	/* Pulse */
	output	pulse
);

`include "log2.v"

localparam integer	PERIOD_WIDTH = 1 + log2( PERIOD );

reg		[PERIOD_WIDTH-1: 0]	count;

always @( posedge clk )
begin
	if( reset )
		begin
			count	<= 0;
		end
	else if( enable )
		begin
			count	<= count + 1;
			if( count >= PERIOD - 1 )
				begin
					count	<= 0;
				end
		end
end

assign	pulse	= ( enable ? ( count == OFFSET ) : 0 );

endmodule

