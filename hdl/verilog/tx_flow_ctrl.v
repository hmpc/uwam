/*
	tx_flow_ctrl.v	- flow control module for transmitter.

	Accepts words at arbitrary data rates, which are then placed in an internal
	FIFO queue and serialised at a constant bit rate. The data may come directly
	from a UART or from some processing core (e.g. Picoblaze). The output
	control signals may be used to control the operation of the other
	transmitter blocks. Disabling the module does not prevent writing to the
	internal FIFO, it only disables serialisation.

	Created:	2014-03-13 16:33:48 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
module tx_flow_ctrl #(
	parameter integer	OUT_PERIOD	= 1_000_000,
	parameter integer	WORD_SIZE	= 8
) (
	/* Module control */
	input	clk,
	input	reset,
	input	enable,

	/* Main I/O */
	input	[WORD_SIZE-1: 0]	pdi,	/* Parallel data in and enable */
	input						pen,

	output						sdo,	/* Serial data out and enable */
	output						sen,

	/* Flow control signals */
	output	cts,						/* Clear to send (parallel) data */
	output	stop						/* No more data to serialise */
);

localparam integer	WORD_PERIOD	= OUT_PERIOD * WORD_SIZE;


/*
	FIFO queue
 */
wire	rd_en;
wire	almost_full;
wire	empty;

wire	[WORD_SIZE-1: 0]	fdo;

tx_fifo fifo (
	.clk( clk ),
	.srst( reset ),
	.din( pdi ),
	.wr_en( pen ),
	.rd_en( rd_en ),
	.dout( fdo ),
	.full( ),
	.almost_full( almost_full ),
	.empty( empty )
);

assign	cts = ~almost_full;

reg		en_word;

always @( posedge clk )
begin
	if( reset )
		begin
			en_word	<= 0;
		end
	else if( rd_en | ~empty )
		begin
			en_word	<= ~empty;
		end
end

assign	stop	= ( enable ? ~en_word : 1'b1 );


/*
	Output and FIFO clocking
 */
wire	clkgen_reset;
reg		en_bit;
reg		rd_word;

assign	clkgen_reset	= reset | ~en_word;

/* Delay serialiser read pulse by FIFO latency (1 clock cycle) */
always @( posedge clk )
begin
	if( reset )
		begin
			rd_word		<= 0;
		end
	else if( enable )
		begin
			rd_word		<= rd_en;
		end
end

/* Generate enable signal for bitrate pulse generator */
always @( posedge clk )
begin
	if( reset )
		begin
			en_bit	<= 0;
		end
	else if( rd_word )
		begin
			en_bit	<= en_word;
		end
end

/* Bit rate clock generator */
pulse_gen #(
	.PERIOD( OUT_PERIOD ),
	.OFFSET( 0 )
) bitrate_gen (
	.clk( clk ),
	.reset( clkgen_reset ),
	.enable( en_bit & enable ),
	.pulse( sen )
);

/* Word rate clock generator */
pulse_gen #(
	.PERIOD( WORD_PERIOD ),
	.OFFSET( 0 )
) wordrate_gen (
	.clk( clk ),
	.reset( clkgen_reset ),
	.enable( en_word & enable ),
	.pulse( rd_en )
);


/*
	Output serialiser
 */
p2s #(
	.WORD_SIZE( WORD_SIZE )
) flow_p2s (
	.clk( clk ),
	.reset( reset ),
	.enable( sen ),
	.read( rd_word ),
	.din( fdo ),
	.dout( sdo )
);

endmodule

