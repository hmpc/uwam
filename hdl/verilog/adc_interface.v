`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    15:36:43 10/08/2010
// Design Name:
// Module Name:    adc_interface
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//		interfaces with AD7912, AD7922, AD7921 etc
//
//		inputs:
//			mode:
//				0x -> get only channel x+1
//				1x -> cycle trough both channels (starting from 1)
//			start: start adc operation (positive pulse)
//			adc_out: adc output (dout)
//		outputs:
//			adc_in, scl, cs: 	to adc
//			ch1rdy: 			got valid conversion in channel 1 (positive pulse)
//			ch2rdy: 			got valid conversion in channel 2 (positive pulse)
//			ch1:	   			channel 1 output (valid when ch1rdy is high)
//			ch2:    			channel 2 output	(valid when ch2rdy is high)
//			adc_error:		high when adc is dead.
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//	TODO: daisy-chain mode
//////////////////////////////////////////////////////////////////////////////////
module adc_interface(
	clk, rst, adc_out, start_in, mode, scl, cs,
	adc_in, ch1, ch2, ch1rdy, ch2rdy, adc_error/*, tquiet, nclk1, nclk0*/
	);

`include "log2.v"

function integer max;
	input integer in1, in2;
	begin
		max = in1 > in2 ? in1 : in2;
	end
endfunction

parameter NCLK1 = 2;		//number of cycles of positive scl
parameter NCLK2 = 2;		//number of cycles of negative scl
parameter NTQUIET = 28;		//number of clock cycles to wait before another convert
parameter NPOWERON = 60;	//number o clock cycles to wait for adc to power on
localparam CLK_DIV_CONST = log2(max(NCLK1, NCLK2)) + 1;
localparam POWER_UP_CONST = log2(max(NTQUIET, NPOWERON)) + 1;

parameter CLK1 = (1 << CLK_DIV_CONST)- NCLK1;
parameter CLK2 = (1 << CLK_DIV_CONST)- NCLK2;
parameter TQUIET = (1 << POWER_UP_CONST) - NTQUIET;
parameter POWERONRST = (1 << POWER_UP_CONST) - NPOWERON;

input clk, rst, adc_out, start_in;
input [1:0] mode;
//input [POWER_UP_CONST-1:0] tquiet;	//DEBUG
//input [7:0] nclk1;	//DEBUG
//input [7:0] nclk0;	//DEBUG
output scl, cs, adc_in, ch1rdy, ch2rdy, adc_error;
output [11:0] ch1, ch2;
reg [11:0] ch1, ch2;
wire ch1rdy, ch2rdy;
//fsm outputs
wire cntrst, waitcntload, updtmode;
reg [4:0] fsm_out;
//fsm inputs
wire dataok, waitok;
//mode
wire ch;
reg [1:0] modeff;
//clkdiv
reg clkdiv;
reg [CLK_DIV_CONST-1:0] clkcnt;
reg [POWER_UP_CONST-1:0] waitcnt;
wire [CLK_DIV_CONST-1:0] clkcntinc;
wire [POWER_UP_CONST-1:0] waitcntinc;
wire cntcout;
//sr
reg [16:0] sr;
wire ch1en, ch2en;
wire start;
wire last_byte;
wire channel_ok;
reg start_ff;
reg [1:0]known_ch;
reg [1:0]ch1rdy_ff, ch2rdy_ff;

assign start = start_in & ~start_ff;
assign last_byte = ~(|sr[16:14]);
assign channel_ok = ~((sr[13] ^ sr[12]) | ((ch ^ sr[13]) & known_ch[1]));
assign dataok = (last_byte & channel_ok & ~cs);
assign scl = clkdiv;
assign ch1en = dataok & ~sr[13];
assign ch2en = dataok & sr[13];
assign ch = ^modeff;
assign adc_in = modeff[0];
assign ch1rdy = ch1rdy_ff[0] && ~(ch1rdy_ff[1]);
assign ch2rdy = ch2rdy_ff[0] && ~(ch2rdy_ff[1]);

always @(posedge clk)
begin
	if(rst) begin
		start_ff <= 0;
	end else begin
		start_ff <= start_in;
	end

	if(rst | cntrst)
		sr <= {{16{1'b1}}, 1'b0};
	else if (cntcout & clkdiv & ~cs)
		sr <= {sr[15:0], adc_out};

	if(rst) begin
		ch1 <= 11'b0;
		ch2 <= 11'b0;
		ch1rdy_ff <= 0;
		ch2rdy_ff <= 0;
	end else begin
		if(ch1en)
			ch1 <=  sr[11:0];

		if(ch2en)
			ch2 <=  sr[11:0];

		ch1rdy_ff <= {ch1rdy_ff[0], ch1en};
		ch2rdy_ff <= {ch2rdy_ff[0], ch2en};
	end

	if(start) begin
		known_ch <= 0;
		modeff <= mode;
	end else if(updtmode & waitok) begin
		modeff[0] <= ch;
		known_ch <= {known_ch[0],1'b1};
	end

end

//clock div

assign {cntcout, clkcntinc} = clkcnt + 1;
assign {waitok, waitcntinc} = waitcnt + 1;

always @(posedge clk)
begin
	if(cntrst | rst) begin
		clkcnt <= CLK1;
		//clkcnt <= nclk1;
	end else if(cntcout) begin
		clkcnt <= clkdiv ? CLK2 : CLK1;
		//clkcnt <= clkdiv ? nclk0 : nclk1;
	end else
		clkcnt <= clkcntinc;

	if(cntrst | rst) begin
		clkdiv <= 1;
	end else begin
		if(cntcout)
			clkdiv <= ~clkdiv;
	end

	if(rst)
	begin
		waitcnt <= POWERONRST; 	//change latter to regulate power on time
	end
	else
	begin
		if(waitcntload)begin
			waitcnt <= TQUIET;
			//waitcnt <= tquiet;
		end else if(!waitok)
			waitcnt <= waitcntinc;
	end
end

//fsm
parameter INIT = 1;
parameter WAIT = 2;
parameter DOWN = 3;
parameter UP = 4;
parameter WAITCLK = 5;
parameter ERROR = 6;

reg [3:0] state = INIT;
always@(posedge clk) begin
	if (rst) begin
		state <= INIT;
	end
	else
	begin
		case(state)
		INIT: 	state <= start ? WAIT : INIT;		//wait a start cmd
		WAIT: 	state <=  waitok ? DOWN : WAIT; 	//wait for power timeout
								//or wait tquiet
		DOWN:	state <= cntcout ? UP : DOWN; 	//wait sclk to go down and rd bit
		UP:					//wait sclk to go up
		begin
			if(cntcout)
			begin
				if(~sr[16])
					state <= dataok ? WAIT/*CLK*/ : ERROR;	//test received data
				else
					state <= DOWN;
			end
			else
				state <= UP;
		end
		/*WAITCLK: begin
			state <= (cntcout & ~clkdiv) ? WAIT : WAITCLK;
		end*/
		ERROR: state <= start ? WAIT : ERROR;			//set error bit
		endcase
	end
end

assign {cntrst, waitcntload, cs, updtmode, adc_error} = fsm_out;
always @* begin
	case (state)
		INIT: fsm_out = 5'b1_0_1_0_0;
		WAIT: fsm_out = 5'b1_0_1_1_0;
		DOWN: fsm_out = 5'b0_0_0_0_0;
		UP: fsm_out = 5'b0_1_0_0_0;
		//WAITCLK: fsm_out = 5'b0_0_1_0_0;
		ERROR: fsm_out = 5'b1_1_1_0_1;
		default: 	fsm_out = INIT;
	endcase
end

endmodule
