`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:        FEUP
// Engineer:       jca@fe.up.pt
//
// Create Date:    17:02:58 04/04/2014
// Design Name:    oho_dy1
// Module Name:    oho_dy1
// Project Name:
// Target Devices: OHO DY1 7 segment display module
// Tool versions:  XILINX ISE
// Description:
//
// Dependencies:   none
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module oho_dy1(
   input clock,    // master clock, active int he posedge
	input reset,    // master synchronous reset, active high
	input [3:0] D0, // right display
	input [3:0] D1, // middle
	input [3:0] D2, // left
	input dp0,      // decimal point for right display
	input dp1,      // middle
	input dp2,      // left

	output reg DY1_SCLK,
	output DY1_SER,
	output reg DY1_RCLK
    );

// clock enable counter
reg [4:0] clken;

// Divide main clock by 32; the serial clock frequency
// is the main clock frequency divided by 64.
// The refresh frequency is the main clock divided by (64*28)
// Fclock   F_SCLK  F_REFRESH
// 150MHz   2.3MHz     83KHz
// 100MHz   781KHz     28KHz
//  20MHz   156KHz      5KHz
always @(posedge clock)
begin
  if ( reset )
  begin
    clken <= 5'd0;
  end
  else
  begin
    clken <= clken + 1;
  end
end

// main clock enable:
assign clken1m = (clken == 0);


// output shift-register:
reg [23:0] output_sr;

// counter of bits out
reg [5:0] cntbits;

// State register:
reg [2:0] state;

// The segments to output packed according to OHO module:
wire [7:0] segs_d0, segs_d1, segs_d2;

// individual segment lines:
wire d0a, d0b, d0c, d0d, d0e, d0f, d0g;
wire d1a, d1b, d1c, d1d, d1e, d1f, d1g;
wire d2a, d2b, d2c, d2d, d2e, d2f, d2g;

// decode from each input digit:
assign {d0a, d0b, d0c, d0d, d0e, d0f, d0g} = hex2sevenseg( D0 );
assign {d1a, d1b, d1c, d1d, d1e, d1f, d1g} = hex2sevenseg( D1 );
assign {d2a, d2b, d2c, d2d, d2e, d2f, d2g} = hex2sevenseg( D2 );

// Align the segmt bits:
assign segs_d0 = { d0e, d0d, d0g, d0a, d0c, dp0, d0b, d0f };
assign segs_d1 = { d1e, d1d, d1g, d1a, d1c, dp1, d1b, d1f };
assign segs_d2 = { d2e, d2d, d2g, d2a, d2c, dp2, d2b, d2f };

// Sequencer & serial data out:
always @(posedge clock)
begin
  if ( reset )
  begin
    output_sr <= 0;
	 cntbits <= 6'd0;
    state <= 0;
	 DY1_SCLK <= 0;
	 DY1_RCLK <= 0;
  end
  else
  begin
    if ( clken1m )
      case( state )
	     0: begin
		       DY1_RCLK <= 1'b1;
		       state <= 1;
			  end

		  1: begin
		       DY1_RCLK <= 1'b0;
		       output_sr <= ~{ segs_d2, segs_d1, segs_d0};
			    cntbits <= 23;
				 state <= 2;
		     end

		  2: begin
		       state <= 3;
			  end

		  3: begin
		       DY1_SCLK <= 1'b1;
				 state <= 4;
			  end

		  4: begin
		       DY1_SCLK <= 1'b0;
		       output_sr <= (output_sr << 1);
			    if ( cntbits == 0 )
			      state <= 0;  // wait for next update trigger
			    else
			    begin
			      cntbits <= cntbits - 1;
			      state <= 3;
			    end
			  end

		default: state <= 0;
	 endcase
  end
end

// drive the output serial data line:
assign DY1_SER = output_sr[23];


// Hex to 7-segment decoder:
function [6:0] hex2sevenseg;
input [3:0] hex;
begin
  case( hex )
    4'b0000: hex2sevenseg = 7'b1111110;
    4'b0001: hex2sevenseg = 7'b0110000;
    4'b0010: hex2sevenseg = 7'b1101101;
    4'b0011: hex2sevenseg = 7'b1111001;
    4'b0100: hex2sevenseg = 7'b0110011;
    4'b0101: hex2sevenseg = 7'b1011011;
    4'b0110: hex2sevenseg = 7'b1011111;
    4'b0111: hex2sevenseg = 7'b1110000;
    4'b1000: hex2sevenseg = 7'b1111111;
    4'b1001: hex2sevenseg = 7'b1111011;
    4'b1010: hex2sevenseg = 7'b1110111;
    4'b1011: hex2sevenseg = 7'b0011111;
    4'b1100: hex2sevenseg = 7'b0001101;
    4'b1101: hex2sevenseg = 7'b0111101;
    4'b1110: hex2sevenseg = 7'b1001111;
    4'b1111: hex2sevenseg = 7'b1000111;
  endcase
end
endfunction

endmodule
