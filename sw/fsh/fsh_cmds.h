/*
	fsh_cmds.h	- FSH command definitions and prototypes

	Created:	2014-04-05 00:36:44 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (elysium)
 */
#ifndef	FSH_CMDS_H
#define	FSH_CMDS_H

#define	REG_READ_MASK	0x80
#define	REG_WRITE_MASK	0x00

#define	GAIN_GET_MASK	0x40
#define	GAIN_SET_MASK	0x00

#define	FRAME_FLAG		0x7E
#define	FRAME_ESC		0x7D
#define	FRAME_XOR		0x20

#define	SEND_BUF_SIZE	1024

#define	ADC_START_MASK	0x20
#define	ADC_CHAN_MASK	0x10
#define	ADC_WINDOW_SIZE	1024

typedef enum {
	FTYPE_CONFIG = 0x01,
	FTYPE_DATA,
	FTYPE_GAIN,
	FTYPE_ADC
} ftype_t;

typedef fsh_ret_t ( *fsh_exec_t )( int argc, char *argv[ ] );

typedef struct fsh_cmd {
	const char	*name;
	fsh_exec_t	exec;
	unsigned	min_args;
	const char	*help;
} fsh_cmd_t;

typedef enum {
	FSH_CMD_HELP = 0,
	FSH_CMD_CONN,
	FSH_CMD_CFG,
	FSH_CMD_SEND,
	FSH_CMD_PUT,
	FSH_CMD_ADC,
	FSH_CMD_DISC,
	FSH_CMD_RUN,
	FSH_CMD_RPT,
	FSH_CMD_DELAY,
	FSH_CMD_QUIT,
	FSH_N_CMDS
} fsh_code_t;

const fsh_cmd_t *fsh_cmd_find( const char *name );

fsh_ret_t	fsh_cmd_help( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_conn( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_cfg( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_send( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_put( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_adc( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_disc( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_run( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_repeat( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_delay( int argc, char *argv[ ] );
fsh_ret_t	fsh_cmd_quit( int argc, char *argv[ ] );

#endif	/* FSH_CMDS_H */

