/*
	fsh.c	- FSK modem shell

	Created:	2014-03-30 14:47:38 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (elysium)
 */
#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>

#include	"linenoise.h"
#include	"serial.h"
#include	"fsh.h"
#include	"fsh_cmds.h"


void print_usage( const char *name ) {
	printf(	"Usage: %s [-h] [-d device] [-l file]\n"
			"\t-h\tdisplay this help.\n"
			"\t-d\tconnect to serial port.\n"
			"\t-l\trun commands in file.\n", name );
}

fsh_ret_t exec_line( char *line ) {
	char			*name;
	const fsh_cmd_t	*cmd;

	name = strtok( line, " \n" );
	if( name == NULL ) {
		return FSH_RET_OK;
	}

	cmd = fsh_cmd_find( name );
	if( cmd == NULL ) {
		return FSH_RET_NOCMD;
	} else {
		char	*args[ MAX_ARGS ];
		char	*a;

		unsigned i;
		for( i = 0; i < MAX_ARGS; ++i ) {
			a = strtok( NULL, " \n" );
			if( a == NULL ) {
				break;
			}
			args[i] = a;
		}

		if( i < cmd->min_args ) {
			return FSH_RET_NARGS;
		} else {
			return cmd->exec( i, args );
		}
	}
}

fsh_ret_t exec_file( const char *fname ) {
	FILE		*in;
	char		line[ MAX_LINE ];
	fsh_ret_t	ret = FSH_RET_OK;
	unsigned	lno = 0;

	in	= fopen( fname, "r" );
	if( in == NULL ) {
		perror( "failed to open file" );
		return FSH_RET_ERR;
	}

	while( fgets( line, MAX_LINE-1, in ) != NULL ) {
		++lno;
		ret	= exec_line( line );
		switch( ret ) {
			case FSH_RET_OK:
				break;

			case FSH_RET_ERR:
				goto cleanup;
				break;

			case FSH_RET_NOCMD:
				fprintf( stderr, "%s:%d: command does not exist.\n",
						fname, lno );
				goto cleanup;
				break;

			case FSH_RET_NARGS:
				fprintf( stderr, "%s:%d: not enough arguments.\n",
						fname, lno );
				goto cleanup;
				break;

			case FSH_RET_QUIT:
				goto cleanup;
				break;

			default:
				break;
		}
	}

cleanup:
	fclose( in );
	return ret;
}

int main( int argc, char *argv[ ] ) {
	int		quit	= 0;
	int		opt;

	while( ( opt = getopt( argc, argv, "hd:l:" ) ) != -1 ) {
		switch( opt ) {
			case 'h':
				print_usage( argv[0] );
				return 0;

			case 'd':
				fsh_cmd_conn( 1, &optarg );
				break;

			case 'l':
				if( exec_file( optarg ) == FSH_RET_QUIT ) {
					quit = 1;
				}
				break;

			default:
				break;
		}
	}

	if( !quit ) {
		linenoiseHistoryLoad( LINE_HISTORY );

		while( !quit ) {
			char		*line = linenoise( PROMPT );
			fsh_ret_t	ret;

			if( line == NULL ) {
				break;
			}
			linenoiseHistoryAdd( line );

			ret = exec_line( line );
			switch( ret ) {
				case FSH_RET_OK:
					break;

				case FSH_RET_ERR:
					break;

				case FSH_RET_NOCMD:
					fprintf( stderr, "command does not exist.\n" );
					break;

				case FSH_RET_NARGS:
					fprintf( stderr, "not enough arguments.\n" );
					break;

				case FSH_RET_QUIT:
					quit = 1;
					break;

				default:
					break;
			}

			free( line );
		}

		linenoiseHistorySave( LINE_HISTORY );
	}

	fsh_cmd_disc( 0, NULL );
	return 0;
}

