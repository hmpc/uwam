/*
	fsh.h		- FSK modem shell definitions

	Created:	2014-04-01 16:10:37 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
#ifndef	FSH_H
#define	FSH_H

#define	PROMPT			"> "
#define	LINE_HISTORY	".fsh_hist"
#define	MAX_ARGS		8
#define	MAX_LINE		1024

typedef	enum {
	FSH_RET_NOCMD = -3,
	FSH_RET_NARGS,
	FSH_RET_ERR,
	FSH_RET_OK,
	FSH_RET_QUIT
} fsh_ret_t;

fsh_ret_t	exec_line( char *line );
fsh_ret_t	exec_file( const char *fname );

#endif	/* FSH_H */

