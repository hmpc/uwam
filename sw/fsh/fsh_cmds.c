/*
	fsh_cmds.c	- fsh command implementation

	Created:	2014-04-05 00:34:43 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (elysium)
 */
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<sys/time.h>
#include	<sys/types.h>
#include	<sys/stat.h>
#include	<sys/wait.h>
#include	<fcntl.h>
#include	<unistd.h>
#include	<signal.h>
#include	<errno.h>
#include	<inttypes.h>

#include	"serial.h"
#include	"fsh.h"
#include	"fsh_cmds.h"


/*
 * Command list
 */
const fsh_cmd_t cmds[ FSH_N_CMDS ] = {
	[FSH_CMD_HELP]	= {	"help",		fsh_cmd_help,	0,	"[<command>]"		},
	[FSH_CMD_CONN]	= {	"connect",	fsh_cmd_conn,	1,	"<device>"			},
	[FSH_CMD_CFG]	= {	"config",	fsh_cmd_cfg,	2,	"reg|gain <reg|instr> [<value>]"	},
	[FSH_CMD_SEND]	= {	"send",		fsh_cmd_send,	1,	"<file>"			},
	[FSH_CMD_PUT]	= { "put",		fsh_cmd_put,	1,	"<byte0> [<byten>]"	},
	[FSH_CMD_ADC]	= { "adc",		fsh_cmd_adc,	1,	"start|stop [<chan>] [<file>]" },
	[FSH_CMD_DISC]	= {	"disc",		fsh_cmd_disc,	0,	""					},
	[FSH_CMD_RUN]	= { "run",		fsh_cmd_run,	1,	"<file>"			},
	[FSH_CMD_RPT]	= { "repeat",	fsh_cmd_repeat,	2,	"<n> <command>"		},
	[FSH_CMD_DELAY]	= { "delay",	fsh_cmd_delay,	1,	"<ms>"				},
	[FSH_CMD_QUIT]	= {	"quit",		fsh_cmd_quit,	0,	""					}
};


/*
 * Global state
 */
int		modem_fd = -1;


/*
 * Helper functions
 */
const fsh_cmd_t *cmd_find( const fsh_cmd_t list[ ], const char *name ) {
	int i;
	for( i = 0; i < FSH_N_CMDS; ++i ) {
		if( strcmp( name, list[i].name ) == 0 ) {
			return &list[i];
		}
	}

	return NULL;
}

int send_frame( int fd, ftype_t type, const uint8_t *data, int len ) {
	const uint8_t flag	= FRAME_FLAG;
	const uint8_t esc	= FRAME_ESC;

	uint8_t byte;
	int ret;
	int i;

	/* Q&D, should stuff all data before sending */
	ret = write( fd, &flag, 1 );
	if( ret == -1 ) {
		return -1;
	}
	ret = write( fd, &type, 1 );
	if( ret == -1 ) {
		return -1;
	}
	for( i = 0; i < len; ++i ) {
		byte = data[i];
		if( byte == FRAME_FLAG || byte == FRAME_ESC ) {
			byte ^= FRAME_XOR;
			ret = write( fd, &esc, sizeof(esc) );
			if( ret == -1 ) {
				return -1;
			}
		}
		ret = write( fd, &byte, sizeof(byte) );
		if( ret == -1 ) {
			return -1;
		}
	}

	return 0;
}

int recv_frame( int fd, ftype_t *type, uint8_t *data ) {
	enum {
		WAIT_STF,
		READ_STF,
		READ_TYPE,
		READ_PAYLOAD,
		DONE
	} state = WAIT_STF;

	uint8_t	byte;
	int		ret;
	int		esc = 0;
	int		i = 0;

	while( state != DONE && ( ret = read( fd, &byte, 1 ) ) != -1 ) {
		if( ret == 0 ) {
			break;
		}

		switch( state ) {
			case WAIT_STF:
				if( byte == FRAME_FLAG ) {
					state = READ_STF;
				}
				break;

			case READ_STF:
				if( byte == FRAME_FLAG ) {
					break;
				} else {
					state = READ_TYPE;
				}

			case READ_TYPE:
				*type = byte;
				state = READ_PAYLOAD;
				break;

			case READ_PAYLOAD:
				if( byte == FRAME_FLAG ) {
					state = DONE;
				} else if( byte == FRAME_ESC ) {
					esc = 1;
				} else if( esc ) {
					data[i] = byte ^ FRAME_XOR;
					++i;
					esc = 0;
				} else {
					data[i] = byte;
					++i;
				}
				break;

			case DONE:
				break;

			default:
				break;
		}
	}

	return ( ret == -1 ? -1 : i );
}

int reg_read( int fd, uint8_t reg, uint16_t *data ) {
	int		ret;
	ftype_t	type;
	uint8_t	in[3] = { 0 };

	reg	= REG_READ_MASK | ( reg & ~REG_READ_MASK );
	ret	= send_frame( fd, FTYPE_CONFIG, &reg, sizeof(reg) );
	if( ret == -1 ) {
		return -1;
	}

	ret	= recv_frame( fd, &type, in );
	if( ret == -1 ) {
		return -1;
	} else if( ret == 0 ) {
		return 0;
	}

	if( type != FTYPE_CONFIG ) {
		return -1;
	}

	*data = in[1] | ( in[2] << 8 );
	return ret;
}

int reg_write( int fd, uint8_t reg, uint16_t data ) {
	uint8_t	out[3];

	out[0]	= REG_WRITE_MASK | ( reg & ~REG_WRITE_MASK );
	out[1]	= ( data & 0x00FF ) >> 0;
	out[2]	= ( data & 0xFF00 ) >> 8;

	return send_frame( fd, FTYPE_CONFIG, out, sizeof(out) );
}

int gain_get( int fd, uint8_t instr, uint8_t *gain ) {
	int		ret;
	ftype_t	type;
	uint8_t	in[2] = { 0 };

	instr = GAIN_GET_MASK | ( instr & ~GAIN_GET_MASK);
	ret	= send_frame( fd, FTYPE_GAIN, &instr, sizeof(instr) );
	if( ret == -1 ) {
		return -1;
	}

	ret	= recv_frame( fd, &type, in );
	if( ret == -1 ) {
		return -1;
	} else if( ret == 0 ) {
		return 0;
	}

	if( type != FTYPE_GAIN ) {
		return -1;
	}

	*gain = in[1];
	return ret;
}

int gain_set( int fd, uint8_t instr, uint8_t gain ) {
	uint8_t	out[2];

	out[0]	= GAIN_SET_MASK | ( instr & ~GAIN_SET_MASK );
	out[1]	= gain;

	return send_frame( fd, FTYPE_GAIN, out, sizeof(out) );
}


/*
 * Main finder
 */
const fsh_cmd_t *fsh_cmd_find( const char *name ) {
	return cmd_find( cmds, name );
}


/*
 * Shell commands
 */
fsh_ret_t fsh_cmd_help( int argc, char *argv[ ] ) {
	const fsh_cmd_t	*cmd;
	if( argc > 0 ) {
		cmd = cmd_find( cmds, argv[0] );
		if( cmd == NULL ) {
			fprintf( stderr, "%s: no such command\n", argv[0] );
			return FSH_RET_ERR;
		}

		printf( "%s %s\n", argv[0], cmd->help );
	} else {
		printf(	"config:\t\tconfigure modem registers\n"
				"connect:\tconnect to modem\n"
				"send:\t\tsend file data to transmit\n"
				"put:\t\tsend bytes to transmit\n"
				"run:\t\trun script file\n"
				"repeat:\t\trepeat command a number of times\n"
				"delay:\t\twait for a given time\n"
				"disc:\t\tdisconnect from modem\n"
				"quit:\t\tquit this shell\n"
				"help:\t\tdisplay this help\n" );
	}

	return FSH_RET_OK;
}

fsh_ret_t fsh_cmd_conn( int argc, char *argv[ ] ) {
	( void )argc;

	if( modem_fd >= 0 ) {
		if( ser_close( modem_fd ) == -1 ) {
			perror( "conn: failed to close previous connection" );
			return FSH_RET_ERR;
		}
	}

	modem_fd = ser_open( argv[0] );
	if( modem_fd < 0 ) {
		perror( "connect: failed to open port" );
		return FSH_RET_ERR;
	}

	if( ser_config( modem_fd ) == -1 ) {
		perror( "connect: failed to configure port" );
		ser_close( modem_fd );
		modem_fd = -1;
		return FSH_RET_ERR;
	}

	return FSH_RET_OK;
}

fsh_ret_t fsh_cmd_cfg( int argc, char *argv[ ] ) {
	int			is_gain;
	uint8_t		reg;
	uint16_t	value;
	uint8_t		gain;
	int			ret;

	if( modem_fd < 0 ) {
		fprintf( stderr, "config: not connected to modem.\n" );
		return FSH_RET_ERR;
	}

	if( strcmp( argv[0], "reg" ) == 0 ) {
		is_gain = 0;
	} else if( strcmp( argv[0], "gain" ) == 0 ) {
		is_gain = 1;
	} else {
		fprintf( stderr, "config: must be either reg or gain.\n" );
		return FSH_RET_ERR;
	}

	reg = strtol( argv[1], NULL, 16 );
	if( errno == ERANGE || errno == EINVAL ) {
		perror( "config: invalid register" );
		return FSH_RET_ERR;
	}

	if( argc > 2 ) {
		value = strtol( argv[2], NULL, 16 );
		if( errno == ERANGE || errno == EINVAL ) {
			perror( "config: invalid value" );
			return FSH_RET_ERR;
		}

		if( is_gain ) {
			ret = gain_set( modem_fd, reg, value );
		} else {
			ret = reg_write( modem_fd, reg, value );
		}

		if( ret == -1 ) {
			perror( "config: failed to set value" );
			return FSH_RET_ERR;
		}
	} else {
		if( is_gain ) {
			ret = gain_get( modem_fd, reg, &gain );
		} else {
			ret = reg_read( modem_fd, reg, &value );
		}

		if( ret == -1 ) {
			perror( "config: failed to get value" );
			return FSH_RET_ERR;
		} else if( ret == 0 ) {
			fprintf( stderr, "config: timeout getting value.\n" );
			return FSH_RET_ERR;
		} else {
			if( is_gain ) {
				printf( "%02X\n", gain );
			} else {
				printf( "%04X\n", value );
			}
		}
	}

	return FSH_RET_OK;
}

fsh_ret_t fsh_cmd_send( int argc, char *argv[ ] ) {
	fsh_ret_t	ret = FSH_RET_OK;
	int			fd;
	ssize_t		rd_cnt;
	uint8_t		buf[ SEND_BUF_SIZE ];

	( void )argc;

	if( modem_fd < 0 ) {
		fprintf( stderr, "send: not connected to modem.\n" );
		return FSH_RET_ERR;
	}

	fd	= open( argv[0], O_RDONLY );
	if( fd == -1 ) {
		perror( "send: failed to open file" );
		return FSH_RET_ERR;
	}

	while( ( rd_cnt = read( fd, &buf[1], SEND_BUF_SIZE-1 ) ) > 0 ) {
		buf[0]	= rd_cnt;
		if( send_frame( modem_fd, FTYPE_DATA, buf, rd_cnt ) == -1 ) {
			perror( "send: failed to write to port" );
			ret = FSH_RET_ERR;
			goto err_cleanup;
		}
	}

	if( rd_cnt == -1 ) {
		perror( "send: failed to read from file" );
		ret = FSH_RET_ERR;
		goto err_cleanup;
	}

err_cleanup:
	close( fd );
	return ret;
}

fsh_ret_t fsh_cmd_put( int argc, char *argv[ ] ) {
	uint8_t	*vals;
	int		i;

	if( modem_fd < 0 ) {
		fprintf( stderr, "put: not connected to modem.\n" );
		return FSH_RET_ERR;
	}

	vals = malloc( (1+argc) * sizeof(*vals) );
	if( vals == NULL ) {
		perror( "put: failed to allocate memory" );
		return FSH_RET_ERR;
	}

	vals[0] = argc;
	for( i = 0; i < argc; ++i ) {
		vals[i+1] = strtol( argv[i], NULL, 16 );
	}

	if( send_frame( modem_fd, FTYPE_DATA, vals, 1+argc ) == -1 ) {
		perror( "put: failed to write to port" );
		free( vals );
		return FSH_RET_ERR;
	}

	free( vals );
	return FSH_RET_OK;
}

fsh_ret_t fsh_cmd_adc( int argc, char *argv[ ] ) {
	static FILE *out = NULL;
	static pid_t child = -1;

	int		chan;
	uint8_t	ctrl = 0x00;

	if( strcmp( argv[0], "start" ) == 0 ) {
		if( child != -1 ) {
			fprintf( stderr, "adc: stop previous conversion first\n" );
			return FSH_RET_ERR;
		}

		if( argc < 3 ) {
			fprintf( stderr, "adc: not enough arguments\n" );
			return FSH_RET_ERR;
		}

		out = fopen( argv[2], "w" );
		if( out == NULL ) {
			perror( "adc: failed to open output file" );
			return FSH_RET_ERR;
		}

		child = fork( );
		if( child == -1 ) {
			perror( "adc: could not fork process" );
			fclose( out );
			return FSH_RET_ERR;
		}

		if( child == 0 ) {
			chan = strtol( argv[1], NULL, 10 );
			ctrl = ADC_START_MASK | ( chan << 4 );
			send_frame( modem_fd, FTYPE_ADC, &ctrl, 1 );

			int			got_lsb = 0;
			ssize_t		ret;
			int			n_sample = 0;
			uint8_t		data;
			int16_t		sample = 0;
			while( ( ret = read( modem_fd, &data, 1 ) ) != -1 ) {
				if( ret != 0 ) {
					if( got_lsb && (data & 0x80) == 0x80 ) {
						got_lsb = 0;
						sample |= ( data ^ 0x80 ) << 7;
						if( sample >= (1<<13) ) {
							sample	-= (1<<14);
						}
						fprintf( out, "%6d\t%5d\n", n_sample, sample );
						++n_sample;
						if( n_sample >= ADC_WINDOW_SIZE ) {
							rewind( out );
							n_sample = 0;
						}
					} else if( (data & 0x80) == 0x00 ) {
						got_lsb = 1;
						sample = data;
					}
				}
			}

			return FSH_RET_ERR;
		} else {
			return FSH_RET_OK;
		}
	} else if( strcmp( argv[0], "stop" ) == 0 ) {
		if( child == -1 ) {
			fprintf( stderr, "adc: no conversion running\n" );
			return FSH_RET_ERR;
		}

		kill( child, SIGTERM );
		if( waitpid( child, NULL, 0 ) == -1 ) {
			perror( "adc: failed to wait for child" );
			return FSH_RET_ERR;
		}
		child = -1;
		fclose( out );
		send_frame( modem_fd, FTYPE_ADC, &ctrl, 1 );

		return FSH_RET_OK;
	} else {
		fprintf( stderr, "adc: must use either start or stop\n" );
		return FSH_RET_ERR;
	}
}

fsh_ret_t fsh_cmd_disc( int argc, char *argv[ ] ) {
	( void )argc;
	( void )argv;

	if( modem_fd >= 0 ) {
		if( ser_close( modem_fd ) == -1 ) {
			perror( "disc: failed to close port" );
			return FSH_RET_ERR;
		}
		modem_fd = -1;
	}

	return FSH_RET_OK;
}

fsh_ret_t fsh_cmd_run( int argc, char *argv[ ] ) {
	( void )argc;
	return exec_file( argv[0] );
}

fsh_ret_t fsh_cmd_repeat( int argc, char *argv[ ] ) {
	const fsh_cmd_t	*cmd;
	fsh_ret_t		ret;
	int				n;

	n = strtol( argv[0], NULL, 10 );
	if( n < 0 ) {
		fprintf( stderr, "repeat: invalid number of repetitions (%d)\n", n );
		return FSH_RET_ERR;
	}

	cmd = fsh_cmd_find( argv[1] );
	if( cmd == NULL ) {
		return FSH_RET_NOCMD;
	} else if( ( unsigned )argc < cmd->min_args + 2 ) {
		return FSH_RET_NARGS;
	}

	int i;
	for( i = 0; i < n; ++i ) {
		ret	= cmd->exec( argc - 2, &argv[2] );
		if( ret != FSH_RET_OK ) {
			break;
		}
	}

	return ret;
}

fsh_ret_t fsh_cmd_delay( int argc, char *argv[ ] ) {
	int			ms;

	( void )argc;

	ms = strtol( argv[0], NULL, 10 );
	if( ms < 0 ) {
		fprintf( stderr, "delay: invalid delay (%d)\n", ms );
		return FSH_RET_ERR;
	}

	if( usleep( ms * 1000 ) == -1 ) {
		perror( "delay" );
		return FSH_RET_ERR;
	}

	return FSH_RET_OK;
}

fsh_ret_t fsh_cmd_quit( int argc, char *argv[ ] ) {
	( void )argc;
	( void )argv;
	return FSH_RET_QUIT;
}

