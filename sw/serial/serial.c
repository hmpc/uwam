/*
	serial.c	- serial port handling functions for UWAM.

	Implements the serial port interface specified in serial.h. Fairly standard,
	POSIX-compliant implementation.

	Created:	2014-03-30 16:44:51 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (elysium)
 */
#include	<stdio.h>
#include	<string.h>
#include	<unistd.h>
#include	<fcntl.h>
#include	<termios.h>
#include	<sys/ioctl.h>
#include	<inttypes.h>

#include	"serial.h"


int ser_open( const char *port ) {
	int	fd;

	fd = open( port, O_RDWR | O_NOCTTY );
	if( fd != -1 ) {
		ser_config( fd );
	}

	return fd;
}

int ser_close( int fd ) {
	return close( fd );
}

int ser_config( int fd ) {
	struct termios	options;

	tcgetattr( fd, &options );

	/* Set baud rate */
	cfsetispeed( &options, SER_BAUD );
	cfsetospeed( &options, SER_BAUD );

	/* Set 8N1 configuration */
	options.c_cflag	&= ~PARENB;
	options.c_cflag	&= ~CSTOPB;
	options.c_cflag	&= ~CSIZE;
	options.c_cflag	|= CS8;

	/* Disable hardware flow control, if available */
#if		defined( CRTSCTS )
	options.c_cflag	&= ~CRTSCTS;
#elif	defined( CNEW_RTSCTS )
	options.c_cflag	&= ~CNEW_RTSCTS;
#endif

	/* Set raw input */
	options.c_lflag	&= ~( ICANON | ECHO | ISIG );

	/* Disable parity checking and software flow control */
	options.c_iflag	&= ~INPCK;
	options.c_iflag	&= ~( IXON | IXANY | IXOFF );

	/* Set raw output */
	options.c_oflag	&= ~OPOST;

	/* Disable blocking read */
	options.c_cc[ VMIN ]	= SER_RD_MIN;
	options.c_cc[ VTIME ]	= SER_RD_TIME;

	/* Flush I/O buffers */
	tcflush( fd, TCIOFLUSH );

	/* Enable receiver and ignore control lines */
	options.c_cflag	|= ( CLOCAL | CREAD );

	return tcsetattr( fd, TCSANOW, &options );
}

int ser_set_rts( int fd, unsigned on ) {
	int	status;

	if( ioctl( fd, TIOCMGET, &status ) == -1 ) {
		return -1;
	}

	if( on ) {
		status	|= TIOCM_RTS;
	} else {
		status	&= ~TIOCM_RTS;
	}

	return ioctl( fd, TIOCMSET, &status );
}

int ser_get_cts( int fd ) {
	int status;

	if( ioctl( fd, TIOCMGET, &status ) == -1 ) {
		return -1;
	}

	return ( status & TIOCM_CTS ? 1 : 0 );
}

