/*
	serial.h	- serial port definitions for UWAM.

	Interface for serial port communication. Open the port using ser_open,
	configure it (ser_config), and then use it as any other file. Reads are
	non-blocking, and will return whether there is data available or not.
	ser_set_mode can be used to change the port mode through the RTS line.

	Created:	2014-03-30 18:42:29 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (elysium)
 */
#ifndef	SERIAL_H
#define	SERIAL_H

#define	SER_BAUD	B115200
#define	SER_RD_MIN	0
#define	SER_RD_TIME	5

int		ser_open( const char *port );
int		ser_close( int fd );
int		ser_config( int fd );
int		ser_set_rts( int fd, unsigned on );
int		ser_get_cts( int fd );

#endif	/*SERIAL_H*/

