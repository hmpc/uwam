/*
	s2p_tb.v	- testbench for serial to parallel converter

	----------------------------------------------------------------------------
	Test						| Expected result
	----------------------------------------------------------------------------
	clock in random data		| the output words match the input and the
								| output clock has the correct width
	----------------------------------------------------------------------------
	reset module				| output goes to zero
	----------------------------------------------------------------------------
	release reset and repeat 1	| same as 1
	----------------------------------------------------------------------------

	Created:	2014-03-12 12:18:50 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module s2p_tb;

`include "transmitter.vh"
`include "log2.v"

/*
	Simulation parameters
 */
parameter	CLK_PERIOD	= 10;

parameter	OUT_WIDTH	= 4;
parameter	OCLK_CYCLES	= 2;

parameter	N_TEST_BITS		= 1024;
parameter	N_TEST_WORDS	= N_TEST_BITS / OUT_WIDTH;

/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;
reg		sck;
reg		sdi;

/* Outputs */
wire						p_clk;
wire	[OUT_WIDTH-1: 0]	p_data;


/*
	Instantiate the UUT
 */
s2p #(
	.OUT_WIDTH( OUT_WIDTH ),
	.OCLK_CYCLES( OCLK_CYCLES )
) uut (
	.clk( clk ),
	.reset( reset ),
	.sck( sck ),
	.sdi( sdi ),
	.out_clk( p_clk ),
	.out_data( p_data )
);


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
reg							in_data [N_TEST_BITS-1: 0];
reg		[OUT_WIDTH-1: 0]	out_words [N_TEST_WORDS-1: 0];

integer	errors = 0;
integer	i;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation of modulator..." );

	/* Initialise inputs */
	reset	= 1;
	sck		= 0;
	sdi		= 0;

	/* Wait for reset to finish */
	repeat( 5 ) @( posedge clk );

	reset	= 0;
	@( posedge clk );

	/* Generate input data */
	for( i = 0; i < N_TEST_BITS; i = i + 1 )
		begin
			in_data[i]			= {$random} % 2;
			out_words[i/4][i%4] = in_data[i];
		end

	$display( "--- 1: clock in random data" );
	for( i = 0; i < N_TEST_BITS; i = i + 1 )
		begin
			sdi		= in_data[i];
			sck		= 1;
			repeat( 2 ) @( posedge clk );
			sck		= 0;
			repeat( 2 ) @( posedge clk );
		end

	$display( "--- 2: reset module" );
	reset	= 1;
	repeat( 2 ) @( posedge clk );

	if( p_data != 0 )
		begin
			$display( "%t\tERROR: output not zero", $realtime );
			errors = errors + 1;
		end

	$display( "--- 3: release reset and repeat 1" );
	reset	= 0;

	/* Generate more input data */
	for( i = 0; i < N_TEST_BITS; i = i + 1 )
		begin
			in_data[i]			= {$random} % 2;
			out_words[i/4][i%4] = in_data[i];
		end

	for( i = 0; i < N_TEST_BITS; i = i + 1 )
		begin
			sdi		= in_data[i];
			sck		= 1;
			repeat( 2 ) @( posedge clk );
			sck		= 0;
			repeat( 2 ) @( posedge clk );
		end

	/* Finish */
	$display( "--- Finished simulation." );
	$display( "--- Got %d error(s).", errors );
	$stop(1);
end

integer	n_words = 0;
reg		[OUT_WIDTH-1: 0]	expected;

always @( posedge p_clk )
begin
	expected	= out_words[ n_words ];
	if( p_data != expected )
		begin
			$display( "%t\tERROR: word %d:\texpected: %b\tgot: %b",
				$realtime, n_words, expected, p_data );
			errors = errors + 1;
		end
	n_words = ( n_words + 1 ) % N_TEST_WORDS;
end

endmodule

