/*
	sdm_tb.v	- testbench for sigma-delta modulator.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	input sawtooth waveform into	| output shows pulse density following the
	modulator						| input waveform
	----------------------------------------------------------------------------

	Created:	2014-03-21 13:56:57 (WET)
	By:			JCA <jca@fe.up.pt>
 */
`timescale 1ns / 10ps

module sdm_tb;

/*
	Simulation parameters
 */
parameter	CLK_PERIOD	= 10;

parameter	N_BITS_IN	= 12;
parameter	SIGNED		= 0;
parameter	SAMPLE_HOLD	= 1;
parameter	CLK_FCY		= 1e9 / CLK_PERIOD;
parameter	SH_FCY		= 1_000_000;


/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;

/* Outputs */
wire	[N_BITS_IN-1: 0]	din;
wire						dout;


/*
	Instantiate the UUT
 */
sdm #(
	.N_BITS_IN( N_BITS_IN ),
	.SIGNED( SIGNED ),
	.SAMPLE_HOLD( SAMPLE_HOLD ),
	.CLK_FCY( CLK_FCY ),
	.SH_FCY( SH_FCY )
) uut (
	.clock( clk ),
	.reset( reset ),
	.din_en( 1'b0 ),
	.din( din ),
	.dout( dout )
);


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Generate reset signal */
initial begin
	reset = 0;
	# 2;
	# 100;
	reset = 1;
	# 100;
	reset = 0;
end

/* Simulation time: 10 periods of 50 KHz signal */
initial begin
	#( CLK_PERIOD * 3000 * 3 );
	$stop;
end

/* Generate a sawtooth wave 50 KHz, count modulo 3000 @ 150 MHz */
reg		[32: 0]		sawtooth;

initial	sawtooth	= 32'd0;
always @(negedge clk)
begin
	if ( sawtooth == 3000-1 )
		sawtooth	<= 0;
	else
		sawtooth	<= sawtooth + 1;
end

/* Scale din for 12 bit full scale, unsigned */
assign	din = ( sawtooth * ((1 << N_BITS_IN) - 1) ) / 3000;

endmodule

