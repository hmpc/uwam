/*
	pulse_gen_tb.v	- testbench for pulse generator.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	enable module and wait for		| 4 pulses with one clock cycle width and
	4*PULSE_PERIOD					| period PULSE_PERIOD
	----------------------------------------------------------------------------
	disable module and wait for		| no pulses are produced
	2*PULSE_PERIOD					|
	----------------------------------------------------------------------------
	enable module, wait for			| no pulses are produced
	PULSE_PERIOD - 1, and disable	|
	----------------------------------------------------------------------------

	Created:	2014-03-14 14:19:47 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module pulse_gen_tb;

/*
	Simulation parameters
 */
parameter	CLK_PERIOD		= 10;
parameter	PULSE_PERIOD	= 8;
parameter	PULSE_OFFSET	= 0;


/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;
reg		enable;

/* Outputs */
wire	pulse;


/*
	Instantiate the UUT
 */
pulse_gen #(
	.PERIOD( PULSE_PERIOD ),
	.OFFSET( PULSE_OFFSET )
) uut (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.pulse( pulse )
);


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset	= 1;
	enable	= 0;

	repeat( 5 ) @( posedge clk );

	$display( "--- 1: enable module and wait for 4*PULSE_PERIOD" );
	reset	= 0;
	enable	= 1;

	repeat( 4*PULSE_PERIOD ) @( posedge clk );

	$display( "--- 2: disable module and wait for 2*PULSE_PERIOD" );
	enable	= 0;

	repeat( 2*PULSE_PERIOD ) @( posedge clk );

	$display( "--- 3: enable module, wait for PULSE_PERIOD - 1, and disable" );
	enable	= 1;
	repeat( PULSE_PERIOD - 1 ) @( posedge clk );
	enable = 0;
	repeat( PULSE_PERIOD ) @( posedge clk );

	/* Finish */
	$display( "--- Finished simulation." );
end

endmodule

