/*
	p2s_tb.v	- testbench for serialiser.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	read in data, enable module		| output matches input data
	----------------------------------------------------------------------------
	read in data, change input		| output matches data read in, not input
	----------------------------------------------------------------------------
	disable module					| output remains constant
	----------------------------------------------------------------------------
	reset module and enable			| output is constant 0
	----------------------------------------------------------------------------

	Created:	2014-03-14 14:50:05 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module p2s_tb;

/*
	Simulation parameters
 */
parameter	CLK_PERIOD		= 10;
parameter	WORD_SIZE		= 8;
parameter	N_TEST_WORDS	= 1024;


/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;
reg		enable;
reg		read;

reg		[WORD_SIZE-1: 0]	din;

/* Outputs */
wire	dout;


/*
	Instantiate the UUT
 */
p2s #(
	.WORD_SIZE( WORD_SIZE )
) uut (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.read( read ),
	.din( din ),
	.dout( dout )
);


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
integer	errors	= 0;
integer	i, j;

reg		prev_dout;
reg		[WORD_SIZE-1: 0]	data [N_TEST_WORDS-1: 0];

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset	= 1;
	enable	= 0;
	read	= 0;
	din		= 0;

	repeat( 5 ) @( posedge clk );

	/* Generate input data */
	for( i = 0; i < N_TEST_WORDS; i = i + 1 )
		begin
			data[i]	= {$random} % (1 << WORD_SIZE);
		end

	$display( "--- 1: read in data, enable module" );
	reset	= 0;
	enable	= 1;

	for( i = 0; i < N_TEST_WORDS; i = i + 1 )
		begin
			din		= data[i];
			read	= 1;
			@( posedge clk );
			read	= 0;

			for( j = 0; j < WORD_SIZE; j = j + 1 )
				begin
					@( posedge clk );
					if( dout != data[i][j] )
						begin
							$display( "%t\tERROR: bit %d: expected %b\tgot %b",
								$realtime, j, data[i][j], dout );
							errors = errors + 1;
						end
				end
		end

	$display( "--- 2: read in data, change input" );
	for( i = 0; i < N_TEST_WORDS; i = i + 1 )
		begin
			din		= data[i];
			read	= 1;
			@( posedge clk );
			read	= 0;
			din		= ~data[i];

			for( j = 0; j < WORD_SIZE; j = j + 1 )
				begin
					@( posedge clk );
					if( dout != data[i][j] )
						begin
							$display( "%t\tERROR: bit %d: expected %b\tgot %b",
								$realtime, j, data[i][j], dout );
							errors = errors + 1;
						end
				end
		end

	$display( "--- 3: disable module" );
	enable		= 0;
	@( posedge clk );
	prev_dout	= dout;

	for( i = 0; i < 10; i = i + 1 )
		begin
			@( posedge clk );
			if( dout != prev_dout )
				begin
					$display( "%t\tERROR: expected %b\tgot %b",
						$realtime, prev_dout, dout );
					errors = errors + 1;
				end
		end

	$display( "--- 4: reset module and enable" );
	reset	= 1;
	@( posedge clk );
	reset	= 0;
	enable	= 1;

	for( i = 0; i < 10; i = i + 1 )
		begin
			@( posedge clk );
			if( dout != 0 )
				begin
					$display( "%t\tERROR: expected %b\tgot %b",
						$realtime, 0, dout );
					errors = errors + 1;
				end
		end

	/* Finish */
	$display( "--- Finished simulation." );
	$display( "--- Got %d errors.", errors );
end

endmodule

