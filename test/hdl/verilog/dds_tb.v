/*
	dds_tb.v	- testbench for DDS module

	----------------------------------------------------------------------------
	Test						| Expected result
	----------------------------------------------------------------------------
	constant inputs on PINC_IN	| constant frequencies on sine
	----------------------------------------------------------------------------

	Created:	2014-03-06 15:01:16 (WET)
	By:			Henrique Cabral <henriquempcabral@gmail.com> (shannon)
 */
`timescale 1ns / 1ps

module dds_tb;

parameter	CLK_PERIOD		= 10;

parameter	DDS_PHASE_SIZE	= 24;
parameter	DDS_OUT_SIZE	= 14;

/* Inputs */
reg							clk;
reg							ce;
reg							sclr;
reg [DDS_PHASE_SIZE-1: 0]	pinc_in;
reg [DDS_PHASE_SIZE-1: 0]	poff_in;

/* Outputs */
wire						rdy;
wire [DDS_PHASE_SIZE-1: 0]	phase_out;
wire [DDS_OUT_SIZE-1: 0]	sine;


/* Instantiate the Unit Under Test (UUT) */
dds uut (
	.clk( clk ),
	.ce( ce ),
	.sclr( sclr ),
	.rdy( rdy ),
	.phase_out( phase_out ),
	.sine( sine ),
	.pinc_in( pinc_in ),
	.poff_in( poff_in )
);


/*
	Helper functions
 */
function [DDS_PHASE_SIZE-1: 0] freq_to_pinc( input real freq );
	freq_to_pinc = (1 << DDS_PHASE_SIZE) * freq * 1e-9*CLK_PERIOD;
endfunction


/*
	Simulation code
 */
parameter	FREQ_MIN	= 15_000;
parameter	FREQ_MAX	= 30_000;
parameter	FREQ_STEP	= 2_500;
parameter	FREQ_TIME	= 1_000_000;

/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
integer freq;
initial begin
	$display( "Starting simulation of DDS module..." );

	/* Initialize inputs */
	ce		= 0;
	sclr	= 1;
	pinc_in	= 0;
	poff_in	= 0;

	/* Wait 10 clock cycles for synchronous clear to finish */
	#( 10 * CLK_PERIOD );

	/* Constant phase increment */
	sclr	= 0;
	ce		= 1;

	for( freq = FREQ_MIN; freq <= FREQ_MAX; freq = freq + FREQ_STEP )
		begin
			pinc_in	= freq_to_pinc( freq );
			#FREQ_TIME;
		end

	$display( "Simulation completed!" );
	$stop;
end

/* Save simulation data */
integer out_file;
initial begin
	$timeformat( -9, 1, "", 12 );
	out_file = $fopen( "../test/data/dds_tb_sine.csv", "wb" );
	$fmonitor( out_file, "%t,%d,%d", $realtime, freq, sine );
end

endmodule

