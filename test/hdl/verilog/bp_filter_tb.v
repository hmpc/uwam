/*
	bp_filter_tb.v	- testbench for polyphase decimator filter.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	apply sinusoids of increasing	| the filter output matches the input
	frequency to filter input		| frequency, and its amplitude grows in the
									| passband
	----------------------------------------------------------------------------

	Created:	2014-05-16 14:30:30 (WEST)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module bp_filter_tb;

/*
	Simulation parameters
 */
parameter	CLK_PERIOD	= 1e9/98.304e6;
parameter	ADC_PERIOD	= 128;
parameter	ADC_WORD_SZ	= 12;

parameter	FREQ_MIN	= 15_000;
parameter	FREQ_MAX	= 35_000;
parameter	FREQ_STEP	= 2_500;
parameter	FREQ_TIME	= 1_000_000;


/*
	I/O ports
 */
/* Inputs */
reg				clk;
reg				reset;
reg				din_rdy;
reg		[11: 0]	din;

/* Outputs */
wire			rfd;
wire			dout_rdy;
wire	[15: 0]	dout;


/*
	Instantiate the UUT
 */
bp_filter filter (
	.clk( clk ),
	.sclr( reset ),
	.nd( din_rdy ),
	.rfd( rfd ),
	.rdy( dout_rdy ),
	.din( din ),
	.dout( dout )
);


/*
	Auxiliary waveform generator and sampling
 */
real			freq;

reg				dds_ce;
reg				dds_sclr;
wire	[23: 0]	pinc_in;

wire			dds_rdy;
wire	[13: 0]	sine;

dds uut (
	.clk( clk ),
	.ce( dds_ce ),
	.sclr( dds_sclr ),
	.rdy( dds_rdy ),
	.phase_out( ),
	.sine( sine ),
	.pinc_in( pinc_in ),
	.poff_in( 24'b0 )
);

assign	pinc_in	= (1 << 24) * freq * 1e-9*CLK_PERIOD;

/* Sample DDS output waveform */
reg		[ 7: 0]	adc_cnt;

always @( posedge clk )
begin
	if( reset )
		adc_cnt	<= 0;
	else if( dds_rdy )
		begin
			adc_cnt	<= adc_cnt + 1;
			if( adc_cnt == ADC_PERIOD-1 )
				begin
					din		<= sine[13:2] + 12'h800;
					din_rdy	<= 1'b1;
					adc_cnt	<= 0;
				end
			else
				begin
					din_rdy	<= 1'b0;
				end
		end
end


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset		= 1;
	din_rdy		= 0;
	din			= 0;
	dds_ce		= 1;
	dds_sclr	= 1;
	freq		= 0.0;

	/* Wait 10 clock cycles for reset and clear to finish */
	#( 10 * CLK_PERIOD );

	reset		= 0;
	dds_sclr	= 0;

	/* Run tests */
	for( freq = FREQ_MIN; freq <= FREQ_MAX; freq = freq + FREQ_STEP )
		#FREQ_TIME;

	/* Finish */
	$display( "--- Finished simulation." );
	$stop;
end

endmodule

