/*
	uwam_top_tb.v	- testbench for UWAM toplevel

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	----------------------------------------------------------------------------

	Created:	2014-03-20 11:04:37 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module uwam_top_tb;

`include "uwam.vh"
`include "transmitter.vh"

/*
	Simulation parameters
 */
parameter	XTAL_PERIOD		= 2e9 / `CLK_FCY;
parameter	CLK_PERIOD		= 1e9 / `CLK_FCY;
parameter	UART_CLK_PERIOD	= 20;

parameter [ 7: 0]	FRAME_FLAG	= 8'h7E;
parameter [ 7: 0]	FRAME_ESC	= 8'h7D;
parameter [ 7: 0]	FRAME_XOR	= 8'h20;

parameter integer	N_SEND		= 4;

parameter integer	PHASE_WIDTH	= `TX_PHASE_WIDTH;
parameter integer	MIN_FREQ	= 21_000;
parameter integer	MAX_FREQ	= 25_500;

parameter	DATA_FILE		= "../test/data/fsk_tx_sync_2.csv";


function [PHASE_WIDTH-1: 0] freq_to_pinc( input real freq );
	freq_to_pinc = (1 << PHASE_WIDTH) * freq * 1e-9*CLK_PERIOD;
endfunction

parameter [PHASE_WIDTH-1: 0]	MIN_PHASE	= freq_to_pinc( MIN_FREQ );
parameter [PHASE_WIDTH-1: 0]	MAX_PHASE	= freq_to_pinc( MAX_FREQ );
parameter [PHASE_WIDTH-1: 0]	RANGE_PHASE	= MAX_PHASE - MIN_PHASE;


/*
	I/O ports
 */
/* Inputs */
reg		xtal;
reg		reset;

/* reg		flash_miso; */
/* reg		adc_dout; */

wire	s3_rx;

/* Outputs */
/* wire	flash_mosi; */
/* wire	flash_sck; */
/* wire	flash_cs; */

/* wire	adc_din; */
/* wire	adc_sck; */
/* wire	adc_cs; */

/* wire	pot_sda; */
/* wire	pot_scl; */

wire	sig_out_n;
wire	sig_out_p;

wire	s3_tx;
wire	cts;


/*
	Instantiate the UUT
 */
uwam_top uut (
	.xtal( xtal ),
	.reset_n( ~reset ),
	.led3_n( ),
	.led8_n( ),
	/* .flash_miso( flash_miso ), */
	/* .flash_miso( flash_miso ), */
	/* .flash_mosi( flash_mosi ), */
	/* .flash_sck( flash_sck ), */
	/* .flash_cs( flash_cs ), */
	/* .adc_dout( adc_dout ), */
	/* .adc_din( adc_din ), */
	/* .adc_sck( adc_sck ), */
	/* .adc_cs( adc_cs ), */
	/* .pot_sda( pot_sda ), */
	/* .pot_scl( pot_scl ), */
	.sig_out_n( sig_out_n ),
	.sig_out_p( sig_out_p ),
	.sig_trig( ),
	.s3_rx( s3_rx ),
	.s3_tx( s3_tx ),
	.s4_tx( cts )
);


/*
	UART for external communication
 */
reg				ser_clk;
reg				ser_tx_en;
reg		[ 7: 0]	ser_tx_data;

wire			ser_tx_rdy;
wire			ser_rx_rdy;
wire	[ 7: 0]	ser_rx_data;

suart_v1 #(
	.INPUT_CLOCK_FREQUENCY( 1e9 / UART_CLK_PERIOD )
) uart (
	.clock( ser_clk ),
	.reset( reset ),
	.tx( s3_rx ),
	.rx( s3_tx ),
	.txen( ser_tx_en ),
	.txready( ser_tx_rdy ),
	.rxready( ser_rx_rdy ),
	.dout( ser_rx_data ),
	.din( ser_tx_data )
);

/* UART tasks */
task ser_write (
	input	[ 7: 0]	data
);
	begin
		@( posedge ser_clk );
		while( ~ser_tx_rdy ) @( posedge ser_clk );
		ser_tx_data	= data;
		ser_tx_en	= 1'b1;
		@( posedge ser_clk );
		ser_tx_en	= 1'b0;
	end
endtask

task ser_read (
	output	[ 7: 0]	data
);
	begin
		@( posedge ser_clk );
		while( ~ser_rx_rdy ) @( posedge ser_clk );
		data		= ser_rx_data;
	end
endtask

task write_stuffed (
	input	[ 7: 0]	data
);
	begin
		if( data == FRAME_FLAG || data == FRAME_ESC )
			begin
				data = data ^ FRAME_XOR;
				ser_write( FRAME_ESC );
			end
		ser_write( data );
	end
endtask

task read_stuffed (
	output	[ 7: 0]	data
);
	begin
		ser_read( data );
		if( data == FRAME_ESC )
			begin
				ser_read( data );
				data	= data ^ FRAME_XOR;
			end
	end
endtask


/*
	Configuration tasks
 */
task send_cfg_hdr;
	begin
		ser_write( FRAME_FLAG );
		write_stuffed( 8'h01 );
	end
endtask

task write_reg (
	input	[ 6: 0]	addr,
	input	[15: 0]	data
);
	begin
		send_cfg_hdr( );
		write_stuffed( { 1'b0, addr } );
		write_stuffed( data[7:0] );
		write_stuffed( data[15:8] );
	end
endtask

task read_reg (
	input	[ 6: 0]	addr,
	output	[15: 0]	data
);
	reg		[ 7: 0]	rd;
	begin
		send_cfg_hdr( );
		write_stuffed( { 1'b1, addr } );
		rd	= ~FRAME_FLAG;
		while( rd != FRAME_FLAG )	ser_read( rd );
		read_stuffed( rd );
		if( rd != 8'h01 )
			begin
				$display( "%t\tERROR: expecting config frame, got type %h",
					$realtime, rd );
			end
		read_stuffed( rd );
		if( rd != addr )
			begin
				$display( "%t\tERROR: reading register %h but got %h",
					$realtime, addr, rd );
			end
		read_stuffed( data[7:0] );
		read_stuffed( data[15:8] );
	end
endtask

task set_sym_pinc (
	input	[15: 0]	sym,
	input	[23: 0]	pinc
);
	begin
		write_reg( `MOD_MAP_SYM, sym );
		write_reg( `MOD_MAP_DTH_L, pinc[15:0] );
		write_reg( `MOD_MAP_DTH_H, { 8'b0, pinc[23:16] } );
	end
endtask

task get_sym_pinc (
	input	[15: 0]	sym,
	output	[23: 0]	pinc
);
	reg		[15: 0]	data;
	begin
		write_reg( `MOD_MAP_SYM, sym );
		read_reg( `MOD_MAP_DTH_L, data );
		pinc[15:0]	= data;
		read_reg( `MOD_MAP_DTH_H, data );
		pinc[23:16]	= data[7:0];
	end
endtask


/*
	Transmission tasks
 */
task send_data_hdr;
	begin
		ser_write( FRAME_FLAG );
		write_stuffed( 8'h02 );
	end
endtask


/*
	Simulation code
 */
/* Generate crystal clock */
initial begin
	xtal = 0;
	forever	xtal = #( XTAL_PERIOD / 2 ) ~xtal;
end

/* Generate UART clock */
initial begin
	ser_clk = 0;
	forever	ser_clk = #( UART_CLK_PERIOD / 2 ) ~ser_clk;
end

/* Run test cases */
reg		[PHASE_WIDTH-1: 0]	pinc_map [`N_SYMBOLS-1: 0];
reg		[PHASE_WIDTH-1: 0]	pinc;
reg		[`CFG_REG_SIZE-1: 0]	reg_data;

integer	errors	= 0;
integer	i;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset		= 1;
	/* flash_miso	= 0; */
	/* adc_dout	= 0; */

	ser_tx_en	= 0;
	ser_tx_data	= 0;

	repeat( 5 ) @( posedge xtal );
	reset		= 0;
	while( uut.reset ) @( posedge xtal );

	/* Initialise modulator frequency map */
	$display( "Initialising pinc map: min=%h\tmax=%h", MIN_PHASE, MAX_PHASE );
	for( i = 0; i < `N_SYMBOLS; i = i + 1 )
		begin
			pinc_map[i]	= MIN_PHASE + RANGE_PHASE*i / (`N_SYMBOLS-1);
			$display( "Symbol %d:\t%h", i, pinc_map[i] );
		end

	/* Program frequency map */
	for( i = 0; i < `N_SYMBOLS; i = i + 1 )
		begin
			set_sym_pinc( i, pinc_map[i] );
		end

	/* Test end-to-end byte stuffing */
	write_reg( 0, 8'h7E );
	read_reg( 0, reg_data );
	if( reg_data != 8'h7E )
		begin
			$display( "%t\tERROR: wrote %h to reg %h, got %h",
				$realtime, 8'h7E, 0, reg_data );
			errors	= errors + 1;
		end

	/* Verify last frequency map entry */
	get_sym_pinc( `N_SYMBOLS-1, pinc );
	if( pinc != pinc_map[`N_SYMBOLS-1] )
		begin
			$display( "%t\tERROR: symbol %h:\texpected: %h\tgot: %h",
				$realtime, i, pinc_map[`N_SYMBOLS-1], pinc );
			errors	= errors + 1;
		end

	$display( "%t\tSending data for transmission...", $realtime );

	/* Send data for transmission */
	send_data_hdr( );
	write_stuffed( N_SEND/2 );
	for( i = 0; i < N_SEND/2; i = i + 1 )
		begin
			/* ser_write( {$random} % (1 << 8) ); */
			write_stuffed( 8'h7E );
		end

	send_data_hdr( );
	write_stuffed( N_SEND/2 );
	for( i = 0; i < N_SEND/2; i = i + 1 )
		begin
			/* ser_write( {$random} % (1 << 8) ); */
			write_stuffed( 8'h55 );
		end

	while( ~uut.tx_stop ) @( posedge xtal );

	/* Finish */
	repeat( 20 ) @( posedge xtal );
	$display( "--- Finished simulation." );
	$display( "--- Got %d errors.", errors );
	$stop;
end

/* Save simulation data */
/* integer out_file; */
/* initial begin */
/* 	$timeformat( -9, 1, "", 12 ); */
/* 	out_file = $fopen( DATA_FILE, "wb" ); */
/* end */

/* always @( posedge uut.clk ) */
/* begin */
/* 	$fwrite( out_file, "%t,%d,%d,%d\n", $realtime, */
/* 		uut.tx.cur_sym, uut.tx_sig_out, sig_out_p ); */
/* end */

endmodule

