/*
	mapper_tb.v	- testbench for mapper module.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	enable module and clock in		| output symbols are Gray-coded and
	words							| correctly clocked
	----------------------------------------------------------------------------
	disable module and clock in		| output symbols match input and sym_clk
	words							| is idle
	----------------------------------------------------------------------------

	Created:	2014-03-18 15:57:27 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module mapper_tb;

`include "log2.v"

/*
	Simulation parameters
 */
parameter integer	CLK_PERIOD	= 10;

parameter integer	N_SYMBOLS	= 8;
parameter integer	SYM_WIDTH	= log2( N_SYMBOLS );
parameter integer	WORD_SIZE	= SYM_WIDTH;

/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;
reg		enable;

reg							word_clk;
reg		[WORD_SIZE-1: 0]	word;

/* Outputs */
wire						sym_clk;
wire	[SYM_WIDTH-1: 0]	symbol;


/*
	Instantiate the UUT
 */
mapper #(
	.SYM_WIDTH( SYM_WIDTH ),
	.WORD_SIZE( WORD_SIZE )
) uut (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.word_clk( word_clk ),
	.word( word ),
	.sym_clk( sym_clk ),
	.symbol( symbol )
);


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
reg		[WORD_SIZE-1: 0]	gray [N_SYMBOLS-1: 0];

integer	errors = 0;
integer	i;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset		= 1;
	enable		= 0;
	word_clk	= 0;
	word		= 0;

	repeat( 5 ) @( posedge clk );

	/* Initialise Gray code */
	gray[0]		= 'b000;
	gray[1]		= 'b001;
	gray[2]		= 'b011;
	gray[3]		= 'b010;
	gray[4]		= 'b110;
	gray[5]		= 'b111;
	gray[6]		= 'b101;
	gray[7]		= 'b100;

	$display( "--- 1: enable module and clock in words" );
	reset	= 0;
	enable	= 1;

	for( i = 0; i < N_SYMBOLS; i = i + 1 )
		begin
			word		= i;
			@( posedge clk );
			word_clk	= 1'b1;
			@( posedge clk );
			word_clk	= 1'b0;
			@( posedge clk );

			if( ~sym_clk )
				begin
					$display( "%t\tERROR: sym_clk failed to assert.",
						$realtime );
					errors	= errors + 1;
				end
			else
				begin
					@( posedge clk );
					if( sym_clk )
						begin
							$display( "%t\tERROR: sym_clk failed to deassert.",
								$realtime );
							errors	= errors + 1;
						end
				end
		end

	$display( "--- 2: disable module and clock in words" );
	enable	= 0;

	for( i = 0; i < N_SYMBOLS; i = i + 1 )
		begin
			word		= i;
			@( posedge clk );
			word_clk	= 1'b1;
			@( posedge clk );
			word_clk	= 1'b0;
			@( posedge clk );
		end

	/* Finish */
	$display( "--- Finished simulation." );
	$display( "--- Got %d errors.", errors );
end

always @( posedge sym_clk )
begin
	if( enable && symbol != gray[word] )
		begin
			$display( "%t\tERROR: symbol: %d\texpected: %b\tgot: %b",
				$realtime, word, gray[word], symbol );
			errors	= errors + 1;
		end
	else if( ~enable && symbol != word )
		begin
			$display( "%t\tERROR: disabled module not transparent for word %d",
				$realtime, word );
			errors	= errors + 1;
		end
end

always @*
begin
	if( ~enable && sym_clk )
		begin
			$display( "%t\tERROR: sym_clk active with module disabled.",
				$realtime );
		end
end

endmodule

