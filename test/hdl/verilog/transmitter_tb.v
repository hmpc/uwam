/*
	transmitter_tb.v	- testbench for transmitter.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	initialise pinc map with		| cfg_dout matches cfg_din after two clock
	uniformly spaced values			| cycles
	----------------------------------------------------------------------------
	enable module and serialise in	| frequency of output signal matches
	random words					| map values for Gray-coded word
	----------------------------------------------------------------------------
	wait for SYMBOL_TIME to elapse	| signal output stops varying
	----------------------------------------------------------------------------

	Created:	2014-03-18 16:57:16 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 100ps

module transmitter_tb;

`include "uwam.vh"
`include "transmitter.vh"
`include "log2.v"

/*
	Simulation parameters
 */
parameter integer	CLK_PERIOD	= 10;

parameter integer	N_SYMBOLS	= 4;
parameter integer	CODE_SIZE	= log2( N_SYMBOLS );
parameter integer	PHASE_WIDTH	= `TX_PHASE_WIDTH;
parameter integer	OUT_WIDTH	= `TX_OUT_WIDTH;
parameter integer	SYMBOL_TIME	= 2_000;

parameter integer	MIN_FREQ	= 100_000;
parameter integer	MAX_FREQ	= 200_000;

parameter integer	N_WORDS		= 256;


/*
	Helper functions
 */
function [PHASE_WIDTH-1: 0] freq_to_pinc( input real freq );
	freq_to_pinc = (1 << PHASE_WIDTH) * freq * 1e-9*CLK_PERIOD;
endfunction

parameter [PHASE_WIDTH-1: 0]	MIN_PHASE	= freq_to_pinc( MIN_FREQ );
parameter [PHASE_WIDTH-1: 0]	MAX_PHASE	= freq_to_pinc( MAX_FREQ );
parameter [PHASE_WIDTH-1: 0]	RANGE_PHASE	= MAX_PHASE - MIN_PHASE;


/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;
reg		enable;

reg		sck;
reg		sdi;

reg								cfg_we;
reg		[`CFG_ADDR_SIZE-1: 0]	cfg_addr;
reg		[`CFG_REG_SIZE-1: 0]	cfg_din;
reg								cfg_update;

/* Outputs */
wire	[`CFG_REG_SIZE-1: 0]	cfg_dout;
wire	[OUT_WIDTH-1: 0]		sig_out;


/*
	Instantiate the UUT
 */
transmitter #(
	.N_SYMBOLS( N_SYMBOLS ),
	.CODE_SIZE( CODE_SIZE ),
	.SYMBOL_TIME( SYMBOL_TIME )
) uut (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.sck( sck ),
	.sdi( sdi ),
	.cfg_we( cfg_we ),
	.cfg_addr( cfg_addr ),
	.cfg_din( cfg_din ),
	.cfg_dout( cfg_dout ),
	.cfg_update( cfg_update ),
	.sig_out( sig_out )
);

/*
	Helper tasks
 */
task config_reg (
	input	[`CFG_ADDR_SIZE-1: 0]	addr,
	input	[`CFG_REG_SIZE-1: 0]	data
);
	begin
		cfg_addr	= addr;
		cfg_din		= data;
		cfg_we		= 1'b1;
		@( posedge clk );
		cfg_we		= 1'b0;
	end
endtask

task config_update;
	begin
		cfg_update	= 1'b1;
		@( posedge clk );
		cfg_update	= 1'b0;
	end
endtask

task ser_in ( input [CODE_SIZE-1: 0] word );
	integer	i;
	begin
		for( i = 0; i < CODE_SIZE; i = i + 1 )
			begin
				sdi		= word[i];
				sck		= 1;
				@( posedge clk );
				sck		= 0;
				@( posedge clk );
			end
	end
endtask


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
reg		[PHASE_WIDTH-1: 0]	freq_map [N_SYMBOLS-1: 0];

integer	i;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset		= 1;
	enable		= 0;
	sck			= 0;
	sdi			= 0;
	cfg_we		= 0;
	cfg_addr	= 0;
	cfg_din		= 0;
	cfg_update	= 0;

	repeat( 5 ) @( posedge clk );
	reset		= 0;

	/* Initialise modulator frequency map */
	$display( "Initialising pinc map: min=%h\tmax=%h", MIN_PHASE, MAX_PHASE );
	for( i = 0; i < N_SYMBOLS; i = i + 1 )
		begin
			freq_map[i]	= MIN_PHASE + RANGE_PHASE*i / N_SYMBOLS;
			$display( "Symbol %d:\t%h", i, freq_map[i] );
		end

	/* Program frequency map into modulator */
	for( i = 0; i < N_SYMBOLS; i = i + 1 )
		begin
			config_reg( `MOD_MAP_SYM, i );
			repeat( 2 ) @( posedge clk );
			config_reg( `MOD_MAP_DTH_L, freq_map[i][ `CFG_REG_SIZE-1: 0 ] );
			repeat( 2 ) @( posedge clk );
			config_reg( `MOD_MAP_DTH_H,
				freq_map[i][ PHASE_WIDTH-1 : `CFG_REG_SIZE ] );
			repeat( 2 ) @( posedge clk );
			config_update;
			@( posedge clk );
		end

	enable	= 1;
	@( posedge clk );

	for( i = 0; i < N_WORDS; i = i + 1 )
		begin
			ser_in( {$random} % N_SYMBOLS );
			#20_000 @( posedge clk );
		end

	/* Finish */
	#5_000 @( posedge clk );
	$display( "--- Finished simulation." );
	$stop;
end

endmodule

