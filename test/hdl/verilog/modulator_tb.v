/*
	modulator_tb.v	- testbench for modulator

	----------------------------------------------------------------------------
	Test								| Expected result
	----------------------------------------------------------------------------
	fill map and test random symbol		| outputs match programmed map values
	sequences							|
	----------------------------------------------------------------------------
	disable module and change input		| output remains constant
	symbol								|
	----------------------------------------------------------------------------
	reenable and reset module and test	| output remains at 0
	random symbol inputs				|
	----------------------------------------------------------------------------
	clock in symbol and let SYMBOL_TIME	| pinc_en goes low after SYMBOL_TIME
	elapse								|
	----------------------------------------------------------------------------

	Created:	2014-03-10 18:36:04 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module modulator_tb;

`include "uwam.vh"
`include "log2.v"

/*
	Simulation parameters
 */
parameter	CLK_PERIOD	= 10;

parameter	N_SYMBOLS	= 8;
parameter	SYM_WIDTH	= log2( N_SYMBOLS );
parameter	PHASE_WIDTH	= `TX_PHASE_WIDTH;
parameter	SYMBOL_TIME	= 16;

parameter	MIN_FREQ	= 15_000;
parameter	MAX_FREQ	= 30_000;

parameter	TEST_LENGTH	= 1024;


/*
	Helper functions
 */
function [PHASE_WIDTH-1: 0] freq_to_pinc( input real freq );
	freq_to_pinc = (1 << PHASE_WIDTH) * freq * 1e-9*CLK_PERIOD;
endfunction

parameter	MIN_PHASE	= freq_to_pinc( MIN_FREQ );
parameter	MAX_PHASE	= freq_to_pinc( MAX_FREQ );
parameter	RANGE_PHASE	= MAX_PHASE - MIN_PHASE;


/*
	I/O ports
 */
/* Inputs */
reg							clk;
reg							reset;
reg							en;
reg							sym_clk;
reg		[SYM_WIDTH-1: 0]	symbol;
reg							update;
reg		[SYM_WIDTH-1: 0]	map_sym;
reg		[PHASE_WIDTH-1: 0]	map_dth;

/* Outputs */
wire						pinc_en;
wire	[PHASE_WIDTH-1: 0]	pinc;


/*
	Instantiate the Unit Under Test (UUT)
 */
modulator #(
	.N_SYMBOLS( N_SYMBOLS ),
	.SYM_WIDTH( SYM_WIDTH ),
	.PHASE_WIDTH( PHASE_WIDTH ),
	.SYMBOL_TIME( SYMBOL_TIME )
) uut (
	.clk( clk ),
	.reset( reset ),
	.enable( en ),
	.sym_clk( sym_clk ),
	.symbol( symbol ),
	.pinc_en( pinc_en ),
	.pinc( pinc ),
	.update( update ),
	.map_sym( map_sym ),
	.map_dth( map_dth )
);


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
reg		[PHASE_WIDTH-1: 0]	test_map [N_SYMBOLS-1: 0];
reg		[PHASE_WIDTH-1: 0]	prev_pinc;
integer						i;
integer						errors = 0;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation of modulator..." );

	/* Initialise inputs */
	reset	= 1;
	en		= 0;
	sym_clk	= 0;
	symbol	= 0;
	update	= 0;
	map_sym	= 0;
	map_dth	= 0;

	/* Wait for reset to finish */
	repeat( 5 ) @( posedge clk );

	reset	= 0;
	@( posedge clk );

	/* Initialise symbol map */
	update	= 1;
	for( i = 0; i < N_SYMBOLS; i = i + 1 )
		begin
			test_map[ i ]	= MIN_PHASE + {$random} % RANGE_PHASE;

			map_sym	= i;
			map_dth	= test_map[ i ];

			repeat( 2 ) @( posedge clk );
		end

	update	= 0;
	@( posedge clk );

	$display( "--- 1: test random symbol sequences" );
	en		= 1;
	for( i = 0; i < TEST_LENGTH; i = i + 1 )
		begin
			symbol	= {$random} % N_SYMBOLS;
			sym_clk	= 1;
			@( posedge clk );
			sym_clk	= 0;
			@( posedge clk );

			if( pinc != test_map[ symbol ] )
				begin
					$display( "%t	ERROR: symbol %d:\texpected: %d\tgot: %d",
						$realtime, symbol, test_map[ symbol ], pinc );
					errors = errors + 1;
				end
		end

	$display( "--- 2: disable module and change input symbol" );
	en			= 0;
	prev_pinc	= pinc;
	for( i = 0; i < TEST_LENGTH; i = i + 1 )
		begin
			symbol	= {$random} % N_SYMBOLS;
			sym_clk	= 1;
			@( posedge clk );
			sym_clk	= 0;
			@( posedge clk );

			if( pinc != prev_pinc )
				begin
					$display( "%t	ERROR: symbol %d:\texpected: %d\tgot: %d",
						$realtime, symbol, prev_pinc, pinc );
					errors = errors + 1;
				end
		end

	$display( "--- 3: reenable, reset, and test random symbol inputs" );
	en		= 1;
	reset	= 1;
	@( posedge clk );
	reset	= 0;
	@( posedge clk );
	for( i = 0; i < TEST_LENGTH; i = i + 1 )
		begin
			symbol	= {$random} % N_SYMBOLS;
			sym_clk	= 1;
			@( posedge clk );
			sym_clk	= 0;
			@( posedge clk );

			if( pinc != 0 )
				begin
					$display( "%t	ERROR: symbol %d:\texpected: %d\tgot: %d",
						$realtime, symbol, 0, pinc );
					errors = errors + 1;
				end
		end

	$display( "--- 4: clock in symbol and let SYMBOL_TIME elapse" );
	update	= 1;
	map_sym	= 0;
	map_dth	= test_map[0];
	@( posedge clk );
	update	= 0;
	symbol	= 0;
	sym_clk	= 1;
	@( posedge clk );
	sym_clk	= 0;

	repeat( SYMBOL_TIME + 1 ) @( posedge clk );
	if( pinc_en )
		begin
			$display( "%t\tERROR: pinc_en still high after SYMBOL_TIME.",
				$realtime );
			errors	= errors + 1;
		end

	/* Finish */
	$display( "--- Finished simulation." );
	$display( "--- Got %d error(s).", errors );
	$stop(1);
end

endmodule

