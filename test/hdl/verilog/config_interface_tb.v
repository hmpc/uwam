/*
	config_interface_tb.v	- testbench for configuration interface.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	send commands to write random	| registers have written values
	data to all registers			|
	----------------------------------------------------------------------------
	send commands to read data from	| data matches values in registers
	all registers					|
	----------------------------------------------------------------------------

	Created:	2014-03-19 18:39:02 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module config_interface_tb;

/*
	Simulation parameters
 */
parameter integer	CLK_PERIOD	= 10;
parameter integer	N_REGS		= 16;


/*
	I/O ports
 */
/* Inputs */
reg				clk;
reg				reset;
reg				rx_new;
reg		[ 7: 0]	rx_data;
reg				tx_rdy;
reg		[15: 0]	reg_din;

/* Outputs */
wire			tx_en;
wire	[ 7: 0]	tx_data;
wire	[ 7: 0]	reg_addr;
wire	[15: 0]	reg_dout;
wire			reg_we;


/*
	Instantiate the UUT
 */
config_interface uut (
	.clk( clk ),
	.reset( reset ),
	.rx_new( rx_new ),
	.rx_data( rx_data ),
	.tx_rdy( tx_rdy ),
	.tx_en( tx_en ),
	.tx_data( tx_data ),
	.reg_addr( reg_addr ),
	.reg_din( reg_din ),
	.reg_dout( reg_dout ),
	.reg_we( reg_we )
);

/*
	Helper tasks
 */
reg		[ 2: 0]	state;
reg		[ 7: 0]	delay_cnt;
reg		[15: 0]	last_data;

always @( posedge clk )
begin
	if( reset )
		begin
			state		<= 0;
			delay_cnt	<= 0;
			last_data	<= 0;
			tx_rdy		<= 0;
		end
	else
		begin
			case( state )
				3'd0:
					begin
						tx_rdy			<= 1'b1;
						if( tx_en )
							begin
								tx_rdy			<= 1'b0;
								last_data[7:0]	<= tx_data;
								delay_cnt		<= 8'b0;
								state			<= 3'd1;
							end
					end

				3'd1:
					begin
						if( delay_cnt >= 8'd20 )
							begin
								tx_rdy			<= 1'b1;
								state			<= 3'd2;
							end
						else
							begin
								delay_cnt		<= delay_cnt + 1;
							end
					end

				3'd2:
					begin
						tx_rdy		<= 1'b1;
						if( tx_en )
							begin
								tx_rdy			<= 1'b0;
								last_data[15:8]	<= tx_data;
								delay_cnt		<= 8'b0;
								state			<= 3'd3;
							end
					end

				3'd3:
					begin
						if( delay_cnt >= 8'd20 )
							begin
								tx_rdy			<= 1'b1;
								state			<= 3'd0;
							end
						else
							begin
								delay_cnt		<= delay_cnt + 1;
							end
					end
			endcase
		end
end

task read_reg (
	input	[ 6: 0]	addr,
	output	[15: 0]	data
);
	begin
		rx_data		= { 1'b1, addr };
		rx_new		= 1'b1;
		@( posedge clk );
		rx_new		= 0;
		while( tx_rdy ) @( posedge clk );
		while( ~tx_rdy ) @( posedge clk );
		while( tx_rdy ) @( posedge clk );
		while( ~tx_rdy ) @( posedge clk );
		data		= last_data;
	end
endtask

task write_reg (
	input	[ 7: 0]	addr,
	input	[15: 0]	data
);
	begin
		rx_data	= { 1'b0, addr };
		rx_new	= 1'b1;
		@( posedge clk );
		rx_data	= data[7:0];
		@( posedge clk );
		rx_data	= data[15:8];
		@( posedge clk );
		rx_new	= 0;
		repeat( 2 ) @( posedge clk );
	end
endtask


/*
	Simulation code
 */
integer	i;

/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Simulate configuration memory */
reg		[15: 0]	mem [N_REGS-1: 0];

always @( posedge clk )
begin
	if( reset )
		begin
			for( i = 0; i < N_REGS; i = i + 1 )
				begin
					mem[i]	<= 0;
				end
		end
	else
		begin
			reg_din	<= mem[ reg_addr ];
			if( reg_we )
				begin
					mem[ reg_addr ]	<= reg_dout;
				end
		end
end

/* Run test cases */
reg		[15: 0]	data [N_REGS-1: 0];
reg		[15: 0]	rd_data;

integer	errors	= 0;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset		= 1;
	rx_new		= 0;
	rx_data		= 0;

	repeat( 5 ) @( posedge clk );
	reset		= 0;

	/* Generate random input data */
	for( i = 0; i < N_REGS; i = i + 1 )
		begin
			data[i]	= {$random} % ( 1 << 16 );
		end

	$display( "--- 1: send commands to write random data to all registers" );
	for( i = 0; i < N_REGS; i = i + 1 )
		begin
			write_reg( i, data[i] );
			if( mem[i] != data[i] )
				begin
					$display( "%t\tERROR: reg %d:\texpected: %h\tgot: %h",
						$realtime, i, data[i], mem[i] );
					errors	= errors + 1;
				end
		end

	$display( "--- 2: send commands to read data from all registers" );
	for( i = 0; i < N_REGS; i = i + 1 )
		begin
			read_reg( i, rd_data );
			if( rd_data != mem[i] )
				begin
					$display( "%t\tERROR: reg %d:\texpected: %h\tgot: %h",
						$realtime, i, mem[i], rd_data );
					errors	= errors + 1;
				end
		end

	/* Finish */
	$display( "--- Finished simulation." );
	$display( "--- Got %d errors.", errors );
	$stop;
end

endmodule

