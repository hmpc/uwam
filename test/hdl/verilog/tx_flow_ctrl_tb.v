/*
	tx_flow_ctrl_tb.v	- testbench for transmitter flow control module.

	----------------------------------------------------------------------------
	Test							| Expected result
	----------------------------------------------------------------------------
	enable module and write test	| the input data is correctly serialised on
	data to FIFO					| sdo/sen, and stop is asserted
	----------------------------------------------------------------------------
	leave FIFO empty for a while	| same as 1
	and then write more data		|
	----------------------------------------------------------------------------
	fill FIFO						| cts is deasserted, input data is correctly
									| serialised
	----------------------------------------------------------------------------
	disable module while it is		| output remains constant and stop is
	serialising data				| asserted
	----------------------------------------------------------------------------
	reenable module					| serialisation continues where it stopped
	----------------------------------------------------------------------------

	Created:	2014-03-17 13:57:25 (WET)
	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
 */
`timescale 1ns / 10ps

module tx_flow_ctrl_tb;

/*
	Simulation parameters
 */
parameter integer	CLK_PERIOD	= 10;
parameter integer	CLK_FCY		= 100_000_000;
parameter integer	BIT_RATE	= 25_000_000;
parameter integer	OUT_PERIOD	= CLK_FCY / BIT_RATE;
parameter integer	WORD_SIZE	= 8;

parameter integer	N_WORDS		= 4096;


/*
	I/O ports
 */
/* Inputs */
reg		clk;
reg		reset;
reg		enable;

reg		[WORD_SIZE-1: 0]	pdi;
reg							pen;

/* Outputs */
wire	sdo;
wire	sen;
wire	cts;
wire	stop;


/*
	Instantiate the UUT
 */
tx_flow_ctrl #(
	.OUT_PERIOD( OUT_PERIOD ),
	.WORD_SIZE( WORD_SIZE )
) uut (
	.clk( clk ),
	.reset( reset ),
	.enable( enable ),
	.pdi( pdi ),
	.pen( pen ),
	.sdo( sdo ),
	.sen( sen ),
	.cts( cts ),
	.stop( stop )
);


/*
	Helper tasks
 */


/*
	Simulation code
 */
/* Generate clock */
initial begin
	clk = 0;
	forever	clk = #( CLK_PERIOD / 2 ) ~clk;
end

/* Run test cases */
reg		[WORD_SIZE-1: 0]	data [N_WORDS-1: 0];
reg		prev_sdo;

integer	errors	= 0;
integer	i;

initial begin
	$timeformat( -9, 1, "", 12 );
	$display( "--- Starting simulation..." );

	/* Initialise inputs */
	reset	= 1;
	enable	= 0;
	pdi		= 0;
	pen		= 0;

	repeat( 5 ) @( posedge clk );

	/* Generate input data */
	for( i = 0; i < N_WORDS; i = i + 1 )
		begin
			data[i]	= {$random} % ( 1 << WORD_SIZE );
		end

	$display( "--- 1: enable module and write test data to FIFO" );
	reset	= 0;
	enable	= 1;
	repeat( 5 ) @( posedge clk );

	for( i = 0; i < N_WORDS / 4; i = i + 1 )
		begin
			pdi		= data[i];
			pen		= 1;
			@( posedge clk );
			pen		= 0;
			@( posedge clk );
		end

	@( posedge stop );

	$display( "--- 2: leave FIFO empty for a while and then write more data" );
	repeat( 5 ) @( posedge clk );

	for( ; i < N_WORDS / 2; i = i + 1 )
		begin
			pdi		= data[i];
			pen		= 1;
			@( posedge clk );
			pen		= 0;
			@( posedge clk );
		end

	@( posedge stop );

	$display( "--- 3: fill FIFO" );
	for( ; i < N_WORDS && cts; i = i + 1 )
		begin
			pdi		= data[i];
			pen		= 1;
			@( posedge clk );
			pen		= 0;
			@( posedge clk );
		end

	if( cts )
		begin
			$display( "%t\tERROR: CTS asserted after FIFO full.", $realtime );
			errors	= errors + 1;
		end

	$display( "--- 4: disable module while it is serialising data" );
	repeat( 10*OUT_PERIOD ) @( posedge clk );
	enable		= 0;
	repeat( 20*OUT_PERIOD )
		begin
			prev_sdo	= sdo;
			@( posedge clk );
			if( sdo != prev_sdo )
				begin
					$display( "%t\tERROR: output changed while disabled.",
						$realtime );
					errors	= errors + 1;
				end
		end

	$display( "--- 5: reenable module" );
	enable		= 1;
	@( posedge stop );

	/* Finish */
	$display( "--- Finished simulation." );
	$display( "--- Got %d errors.", errors );
end

/* Verify serial output */
integer	j	= 0;
reg		expected;

always @( posedge sen )
begin
	expected	= data[ j/WORD_SIZE ][ j%WORD_SIZE ];
	if( expected != sdo )
		begin
			$display( "%t\tERROR: word%d:\texpected: %b\tgot: %b",
				$realtime, j/WORD_SIZE, expected, sdo );
			errors	= errors + 1;
		end

	j	= j + 1;
end

endmodule

