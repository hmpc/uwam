#
#	tx_flow_ctrl_tb.do	- Questa Simulator script for transmitter flow control.
#
#	Created:	2014-03-17 14:02:58 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  "../ipcore_dir/tx_fifo.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/tx_flow_ctrl.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/tx_flow_ctrl_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.tx_flow_ctrl_tb glbl

# Source the wave do file
do {../test/wave/tx_flow_ctrl_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 8000000ns

# End

