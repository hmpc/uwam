#
#	bp_filter_tb.do	- Questa Simulator script for FIR filter testbench
#
#	Created:	2014-05-16 15:19:54 (WEST)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  "../ipcore_dir/dds.v"
vlog  "../ipcore_dir/bp_filter.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/bp_filter_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.bp_filter_tb glbl

# Source the wave do file
do {../test/wave/bp_filter_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 10000000ns

# End

