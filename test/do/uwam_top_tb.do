#
#	uwam_top_tb.do	- Questa Simulator script for UWAM toplevel.
#
#	Created:	2014-03-20 11:11:31 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  "dcm_2x.v"
vcom  "../hdl/vhdl/kcpsm3.vhd"
vlog  +incdir+../hdl/verilog "../ipcore_dir/dds.v"
vlog  +incdir+../hdl/verilog "../ipcore_dir/tx_fifo.v"
vlog  +incdir+../hdl/verilog "../ipcore_dir/pb_fifo.v"
vlog  +incdir+../hdl/verilog "../pb/pb_rom.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/i2c.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/s2p.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/pulse_gen.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/p2s.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/modulator.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/mapper.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/tx_flow_ctrl.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/transmitter.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/suart_v1.v"
vlog  +incdir+../hdl/verilog "../hdl/verilog/uwam_top.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/uwam_top_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.uwam_top_tb glbl

# Source the wave do file
do {../test/wave/uwam_top_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 80000000ns

# End

