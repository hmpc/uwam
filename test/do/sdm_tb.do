#
#	sdm_tb.do	- Questa Simulator script for sigma-delta modulator testbench
#
#	Created:	2014-03-21 14:03:45 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  +incdir+../hdl/verilog "../hdl/verilog/sdm.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/sdm_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.sdm_tb glbl

# Source the wave do file
do {../test/wave/sdm_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 8000000ns

# End

