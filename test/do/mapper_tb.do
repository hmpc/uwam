#
#	mapper_tb.do	- Questa Simulator script for mapper module testbench.
#
#	Created:	2014-03-18 16:16:28 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  +incdir+../hdl/verilog "../hdl/verilog/mapper.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/mapper_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.mapper_tb glbl

# Source the wave do file
do {../test/wave/mapper_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 8000000ns

# End

