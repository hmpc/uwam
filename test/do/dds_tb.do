#
#	dds_tb.do	- Questa Simulator script for DDS testbench
#
#	Created:	2014-03-05
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  "../ipcore_dir/dds.v"
vlog  "../test/hdl/verilog/dds_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.dds_tb glbl

# Source the wave do file
do {../test/wave/dds_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 8000000ns

# End

