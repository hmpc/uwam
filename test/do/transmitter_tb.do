#
#	transmitter_tb.do	- Questa Simulator script for transmitter testbench.
#
#	Created:	2014-03-18 17:15:54 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  +incdir+../hdl/verilog "../hdl/verilog/transmitter.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/transmitter_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.transmitter_tb glbl

# Source the wave do file
do {../test/wave/transmitter_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 30000000ns

# End

