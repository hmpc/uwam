#
#	s2p_tb.do	- Questa Simulator script for S2P testbench
#
#	Created:	2014-03-12 13:38:33 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  +incdir+../hdl/verilog "../hdl/verilog/s2p.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/s2p_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.s2p_tb glbl

# Source the wave do file
do {../test/wave/s2p_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 8000000ns

# End

