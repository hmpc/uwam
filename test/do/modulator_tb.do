#
#	modulator_tb.do	- Questa Simulator script for modulator testbench
#
#	Created:	2014-03-11 16:04:41 (WET)
#	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)

# Create work library
vlib work

# Compile sources
vlog  +incdir+../hdl/verilog "../hdl/verilog/modulator.v"
vlog  +incdir+../hdl/verilog "../test/hdl/verilog/modulator_tb.v"
vlog  "$::env(XILINX)/verilog/src/glbl.v"

# Call vsim to invoke simulator
vsim -voptargs="+acc" -t 1ps  -L xilinxcorelib_ver -L unisims_ver -L unimacro_ver -lib work work.modulator_tb glbl

# Source the wave do file
do {../test/wave/modulator_wave.do}

# Set the window types
view wave
view structure
view signals

# Run simulation
run 8000000ns

# End

