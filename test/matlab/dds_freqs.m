function [ nfreq, mfreq, fwhm ] = dds_freqs( data )

	nfreq = unique( data(:,2) );
	mfreq = zeros( 1, length(nfreq) );
	fwhm = zeros( 1, length(nfreq) );

	for i = 1:length(nfreq)
		idx = find( data(:,2) == nfreq(i) );
		out = data( idx, [1 3] );
		len = length(idx);

		y = zeros(1,len);
		for j = 1:len
			if( out(j,2) >= 2^13 )
				y(j) = out(j,2) - 2^14;
			else
				y(j) = out(j,2);
			end
			y(j) = y(j) / 2^13;
		end

		tnu = out(:,1) * 1e-9;
		Ts = mean( diff(tnu) );
		Fs = 1/Ts;

		tu = tnu(1):Ts:tnu(end);
		yi = interp1( tnu, y, tu );

		nfft = 10*length(yi);
		spectrum = fft( yi, nfft );

		power = abs( spectrum( 1:floor( nfft/2 ) ) ).^2;
		freqs = Fs/nfft * ( 0:floor( nfft/2 )-1 );

		[M,m] = max(power);
		power = power / M;
		mfreq(i) = freqs(m);

		lm = max( find( power(1:m-1) < sqrt(.5) ) );
		hm = min( find( power(m+1:end) < sqrt(.5) ) );
		fwhm(i) = freqs(hm+m) - freqs(lm);
	end

end

