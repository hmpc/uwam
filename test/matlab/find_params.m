function [ fs, fsym, tsym ] = find_params

	% Constants
	CLK_FCY		= 98.304e6;			% Clock frequency
	FSYM_RES	= CLK_FCY / 2^24;	% Symbol frequency resolution (from DDS)
	RX_M		= 8;				% Decimation rate of RX filter
	FFT_N		= 256;				% Size of FFT
	FS_TOL		= 1e-9;				% Fs tolerance (relative to CLK_FCY)

	% Constraints
	MIN_RATE	= 500;				% Minimum symbol rate
	MIN_FRQ		= 5;				% Minimum symbol frequency/rate ratio
	MIN_FS		= RX_M*2*40e3;		% Minimum sampling frequency
	MAX_FS		= 1e6;				% Maximum sampling frequency
	MIN_SFREQ	= 20e3;				% Minimum symbol frequency
	MAX_SFREQ	= 27e3;				% Maximum symbol frequency

	%%%

	% Initialisation
	fs		= [];
	fsym	= [];
	tsym	= [];

	% Symbol frequency iteration
	start_i	= ceil(MIN_SFREQ / FSYM_RES);
	end_i	= floor(MAX_SFREQ / FSYM_RES);
	for i = start_i:end_i
		freq	= FSYM_RES * i;

		% Symbol rate iteration
		start_j	= floor( freq / MIN_RATE );
		end_j	= MIN_FRQ;
		for j = start_j:-1:end_j
			rate	= freq / j;

			% Window size (L) iteration
			start_l	= nextpow2( ceil( MIN_FS / (RX_M*rate) ) );
			end_l	= min( floor( log2( [ FFT_N (MAX_FS/(RX_M*rate)) ] ) ) );

			if end_l < start_l
				continue
			end

			for l = start_l:end_l
				div	= CLK_FCY / ( rate * RX_M * 2^l );

				% Check for solution
				if abs(div - floor(div)) < FS_TOL
					fs		= [ fs		(rate*RX_M*2^l)	];
					fsym	= [ fsym	freq			];
					tsym	= [ tsym	(1/rate)		];
				end
			end
		end
	end

end

