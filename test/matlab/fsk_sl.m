%
%	fsk_sl.m	- Simulink model setup for FSK modem
%
%	Created:	2014-03-24 12:29:00 (WET)
%	By:			Henrique Cabral <henrique.cabral@fe.up.pt> (shannon)
%

% Load TX simulation data
full_data	= load( '../data/fsk_tx_sync_2.csv' );
data		= full_data( (min( find( full_data(:,3) ~= 0 ) )):end, : );

tx_time		= ( data( :, 1 ) - data( 1, 1 ) ) * 1e-9;
tx_sym		= data( :, 2 );
tx_sig		= data( :, 3 );
tx_out		= data( :, 4 );

for i = 1:length(tx_sig)
	if( tx_sig(i) > 2^13-1 )
		tx_sig(i)	= tx_sig(i) - 2^14;
	end
end

% Set up transmitter parameters
tx_nsym		= 2;
tx_tsym		= 65536 / 98.304e6;
tx_freqs	= [ 21 25.5 ] * 1e3;

% Build transducer model for transmitter (S/N 18913)
trans_CA	= 1.1e-9;
trans_LA	= 42.5e-3;
trans_RA	= 2445.0;
trans_C0	= 10.536e-9;
tx_T		= tf( [ trans_RA 0 ], [ trans_LA trans_RA 1/trans_CA ] );

% Set up receiver parameters
rx_Fs		= 768e3;
rx_Ts		= 1/rx_Fs;
rx_M		= 8;
rx_L		= 64;
rx_I		= 4;
rx_inbw		= [ 20 30 ] * 1e3;
rx_firb		= fir1( 64, rx_inbw * 2/rx_Fs );
rx_noff		= 8;
rx_bins		= tx_freqs * (rx_L*rx_I) * rx_M/rx_Fs;

% Set up channel parameters
ch_sprel	= 0;
ch_spsound	= 1500;

ch_noise	= powernoise( 1.8, length(tx_time)+1, 'normalize', 'randpower' );
ch_noise	= ch_noise(1:end-1);
ch_noise	= ( ch_noise - mean(ch_noise) ) / 2;

% Clean up
clear full_data data i

