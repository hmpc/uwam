onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /dds_tb/clk
add wave -noupdate /dds_tb/ce
add wave -noupdate /dds_tb/sclr
add wave -noupdate -radix decimal /dds_tb/pinc_in
add wave -noupdate -radix decimal /dds_tb/poff_in
add wave -noupdate /dds_tb/rdy
add wave -noupdate -radix decimal /dds_tb/phase_out
add wave -noupdate -radix unsigned /dds_tb/freq
add wave -noupdate -format Analog-Step -height 84 -max 8191.0 -min -8192.0 /dds_tb/sine
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7000099518 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {7350105 ns}
