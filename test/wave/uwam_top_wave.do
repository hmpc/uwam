onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /uwam_top_tb/uut/clk
add wave -noupdate /uwam_top_tb/xtal
add wave -noupdate /uwam_top_tb/uut/reset_n
add wave -noupdate /uwam_top_tb/reset
add wave -noupdate /uwam_top_tb/uut/pb_addr
add wave -noupdate /uwam_top_tb/ser_clk
add wave -noupdate /uwam_top_tb/ser_rx_data
add wave -noupdate /uwam_top_tb/ser_rx_rdy
add wave -noupdate /uwam_top_tb/ser_tx_data
add wave -noupdate /uwam_top_tb/ser_tx_en
add wave -noupdate /uwam_top_tb/ser_tx_rdy
add wave -noupdate /uwam_top_tb/uut/uart_cts
add wave -noupdate /uwam_top_tb/uut/uart_rx
add wave -noupdate /uwam_top_tb/uut/uart_rx_data
add wave -noupdate /uwam_top_tb/uut/uart_rx_rdy
add wave -noupdate /uwam_top_tb/uut/uart_tx
add wave -noupdate /uwam_top_tb/uut/uart_tx_data
add wave -noupdate /uwam_top_tb/uut/uart_tx_en
add wave -noupdate /uwam_top_tb/uut/uart_tx_rdy
add wave -noupdate /uwam_top_tb/uut/tx_stop
add wave -noupdate -format Analog-Step -height 84 -max 8192.0 -min -8192.0 /uwam_top_tb/uut/tx_sig_out
add wave -noupdate /uwam_top_tb/sig_out_p
add wave -noupdate /uwam_top_tb/sig_out_n
add wave -noupdate /uwam_top_tb/uut/tx_cfg_addr
add wave -noupdate /uwam_top_tb/uut/tx_cfg_din
add wave -noupdate /uwam_top_tb/uut/tx_cfg_dout
add wave -noupdate /uwam_top_tb/uut/tx_cfg_we
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3290653584 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 287
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {4033878125 ps}
