onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /mapper_tb/clk
add wave -noupdate /mapper_tb/enable
add wave -noupdate /mapper_tb/reset
add wave -noupdate /mapper_tb/sym_clk
add wave -noupdate /mapper_tb/symbol
add wave -noupdate /mapper_tb/word
add wave -noupdate /mapper_tb/word_clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 172
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {7999999050 ps} {8000000025 ps}
