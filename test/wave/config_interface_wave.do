onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /config_interface_tb/clk
add wave -noupdate /config_interface_tb/reset
add wave -noupdate /config_interface_tb/rx_new
add wave -noupdate /config_interface_tb/rx_data
add wave -noupdate /config_interface_tb/tx_en
add wave -noupdate /config_interface_tb/tx_rdy
add wave -noupdate /config_interface_tb/tx_data
add wave -noupdate /config_interface_tb/reg_addr
add wave -noupdate /config_interface_tb/reg_din
add wave -noupdate /config_interface_tb/reg_dout
add wave -noupdate /config_interface_tb/reg_we
add wave -noupdate /config_interface_tb/state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 4} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 226
configure wave -valuecolwidth 73
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {8951250 ps}
