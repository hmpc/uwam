onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /transmitter_tb/clk
add wave -noupdate /transmitter_tb/reset
add wave -noupdate /transmitter_tb/enable
add wave -noupdate /transmitter_tb/sck
add wave -noupdate /transmitter_tb/sdi
add wave -noupdate /transmitter_tb/cfg_we
add wave -noupdate /transmitter_tb/cfg_addr
add wave -noupdate /transmitter_tb/cfg_din
add wave -noupdate /transmitter_tb/cfg_dout
add wave -noupdate /transmitter_tb/cfg_update
add wave -noupdate /transmitter_tb/uut/mod_update
add wave -noupdate -radix unsigned /transmitter_tb/uut/mod_map_sym
add wave -noupdate -radix hexadecimal /transmitter_tb/uut/mod_map_dth
add wave -noupdate -format Analog-Step -height 84 -max 8192.0 -min -8192.0 /transmitter_tb/sig_out
add wave -noupdate /transmitter_tb/uut/code_sck
add wave -noupdate /transmitter_tb/uut/code_sda
add wave -noupdate /transmitter_tb/uut/word_clk
add wave -noupdate /transmitter_tb/uut/cur_word
add wave -noupdate /transmitter_tb/uut/sym_clk
add wave -noupdate /transmitter_tb/uut/cur_sym
add wave -noupdate /transmitter_tb/uut/dds_ce
add wave -noupdate /transmitter_tb/uut/dds_rdy
add wave -noupdate -radix hexadecimal /transmitter_tb/uut/dds_pinc
add wave -noupdate -radix unsigned /transmitter_tb/uut/dds_poff
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 258
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {337032610 ps}
