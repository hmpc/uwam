onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /modulator_tb/clk
add wave -noupdate /modulator_tb/reset
add wave -noupdate /modulator_tb/en
add wave -noupdate /modulator_tb/sym_clk
add wave -noupdate /modulator_tb/symbol
add wave -noupdate /modulator_tb/update
add wave -noupdate /modulator_tb/map_sym
add wave -noupdate /modulator_tb/map_dth
add wave -noupdate /modulator_tb/pinc_en
add wave -noupdate /modulator_tb/pinc
add wave -noupdate /modulator_tb/uut/new_sym
add wave -noupdate /modulator_tb/uut/sym_time
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 194
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {64968750 ps}
