onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tx_flow_ctrl_tb/clk
add wave -noupdate /tx_flow_ctrl_tb/reset
add wave -noupdate /tx_flow_ctrl_tb/enable
add wave -noupdate /tx_flow_ctrl_tb/pen
add wave -noupdate /tx_flow_ctrl_tb/pdi
add wave -noupdate /tx_flow_ctrl_tb/cts
add wave -noupdate /tx_flow_ctrl_tb/uut/rd_en
add wave -noupdate /tx_flow_ctrl_tb/uut/rd_word
add wave -noupdate /tx_flow_ctrl_tb/sen
add wave -noupdate /tx_flow_ctrl_tb/sdo
add wave -noupdate /tx_flow_ctrl_tb/uut/empty
add wave -noupdate /tx_flow_ctrl_tb/uut/en_word
add wave -noupdate /tx_flow_ctrl_tb/stop
add wave -noupdate -radix unsigned /tx_flow_ctrl_tb/errors
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {465017668 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 222
configure wave -valuecolwidth 66
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {8390106007 ps}
