onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /bp_filter_tb/clk
add wave -noupdate /bp_filter_tb/reset
add wave -noupdate /bp_filter_tb/freq
add wave -noupdate /bp_filter_tb/rfd
add wave -noupdate /bp_filter_tb/adc_cnt
add wave -noupdate -format Analog-Step -height 84 -max 2046.0000000000002 -min -2047.0 /bp_filter_tb/din
add wave -noupdate /bp_filter_tb/din_rdy
add wave -noupdate -format Analog-Step -height 84 -max 2169.0000000000005 -min -2014.0 /bp_filter_tb/dout
add wave -noupdate /bp_filter_tb/dout_rdy
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 197
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {9450106817 ps}
