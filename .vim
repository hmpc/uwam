au BufRead,BufNewFile *.vh	setfiletype verilog

let b:get_name	= 'git config --get user.name'
let b:get_email	= 'git config --get user.email'
let b:header	= '.header'

nnoremap <buffer> fh :call FileHeader_Include( )<CR>

if exists("*FileHeader_Include")
	finish
endif

function FileHeader_Include( )
	let header_file = findfile( b:header, '.;' )
	if header_file == ''
		echoerr 'No header file (' . b:header . ') found!'
		return 1
	endif

	echom 'Using header: ' . header_file
	let start = line('.')
	exe "source " . header_file
	let last = line('.')
	let range = start . ',' . last

	exe 'silent! ' . range . 's/<file>/' . expand('%:t') . '/'
	exe 'silent! ' . range . 's/<timestamp>/' . strftime('%F %T (%Z)') . '/'
	exe 'silent! ' . range . 's/<name>/' . system('echo -n $(' . b:get_name . ')') . '/'
	exe 'silent! ' . range . 's/<email>/' . system('echo -n $(' . b:get_email . ')') . '/'
	exe 'silent! ' . range . 's/<host>/' . hostname( ) . '/'
	exe 'silent! ' . range . 's/<here>//'
endfunction

